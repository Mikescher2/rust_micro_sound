#![no_std]
#![no_main]
#![feature(alloc_error_handler)]
#![feature(alloc)]

extern crate micro_sound;

#[macro_use]
extern crate alloc;
extern crate cortex_m_semihosting as sh;
extern crate stm32f7;
extern crate stm32f7_discovery;

use core::alloc::Layout as AllocLayout;
use core::panic::PanicInfo;
use cortex_m::asm;
use cortex_m_rt::{entry, exception};
use sh::hio;
use stm32f7_discovery::*;

use alloc_cortex_m::CortexMHeap;

#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();
const HEAP_SIZE: usize = 50 * 1024; // in bytes
const DOUBLE_BUFFERED: bool = true;

use micro_sound::osc::{Osc, RECORDER};

#[entry]
fn main() -> ! {
    let mut osc = Osc::new(&ALLOCATOR, HEAP_SIZE, DOUBLE_BUFFERED);
    osc.run()
}

#[exception]
fn SysTick() {
    system_clock::tick();

    unsafe {
        if let Some(rec) = &mut RECORDER {
            let (rec_value, num_writes) = rec.get();
            let value = micro_sound::audio::AudioMan::play_tick(rec_value, num_writes);
            rec.update(value);
        }
    }
}

// define what happens in an Out Of Memory (OOM) condition
#[alloc_error_handler]
fn rust_oom(_: AllocLayout) -> ! {
    loop {
        system_clock::wait_ticks(1);
    }
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    if lcd::stdout::is_initialized() {
        println!("{}", info);
    }

    if let Ok(mut hstdout) = hio::hstdout() {
        let strout = format!("{}", info);
        hstdout.write_all(strout.as_bytes()).expect("hstdout ??");
    }

    // OK to fire a breakpoint here because we know the microcontroller is connected to a debugger
    asm::bkpt();

    loop {
        system_clock::wait_ticks(1);
    }
}
