use core::ptr;

#[derive(Copy, Clone)]
pub struct RamBuffer {
    pub start: u32,
    pub len: usize,
}

impl RamBuffer {
    pub const fn new(start: u32, len: usize) -> Self {
        RamBuffer { start, len }
    }

    pub fn set(self, index: usize, value: u32) {
        let ptr1 = (self.start + ((index as u32) * 4) % self.len as u32) as *mut u32;
        unsafe {
            ptr::write_volatile(ptr1, value);
        }
    }

    pub fn get(self, index: usize) -> u32 {
        let ptr1 = (self.start + ((index as u32) * 4) % self.len as u32) as *mut u32;
        unsafe { ptr::read_volatile(ptr1) }
    }
}
