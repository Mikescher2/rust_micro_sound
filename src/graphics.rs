#![allow(dead_code)]

const DEBUG_REPAINT: bool = false;

use math;
use stm32f7_discovery::lcd::*;
use stm32f7_discovery::*;

use crate::rambuffer::*;
use crate::util::*;

pub struct Graphics {
    layer1: Layer<stm32f7_discovery::lcd::FramebufferArgb8888>,

    buffer_enabled: bool,
    buffer_value: RamBuffer,
    buffer_dirty: [[bool; 17]; 30],

    transformation_inverse: Matrix3x3,
    transformation: Matrix3x3,

    transformation_backup: [Option<(Matrix3x3, Matrix3x3)>; 4],
    transformation_backup_index: usize,

    overlay: bool,
    overlay_grid_x: i32,
    overlay_grid_y: i32,
    overlay_grid_d: i32,
    overlay_grid_c: u32,
}

impl Graphics {
    pub fn new(layer: Layer<stm32f7_discovery::lcd::FramebufferArgb8888>, buffy: bool) -> Self {
        Self {
            transformation: Matrix3x3::identity(),
            transformation_inverse: Matrix3x3::identity(),
            layer1: layer,

            buffer_value: RamBuffer::new(0xC010_0000, 472 * 800 * 4), // 0xC010_0000 .. 0xC027_0C00
            buffer_dirty: [[false; 17]; 30],
            buffer_enabled: buffy,

            transformation_backup: [None; 4],
            transformation_backup_index: 0,

            overlay: false,
            overlay_grid_x: 0,
            overlay_grid_y: 0,
            overlay_grid_d: 0,
            overlay_grid_c: 0x0,
        }
    }

    fn transform_point(&self, x: f32, y: f32) -> (i32, i32) {
        let t = self.transformation.mult((x, y));
        (math::roundf(t.0) as i32, math::roundf(t.1) as i32)
    }

    fn inverse_transform_point(&self, x: i32, y: i32) -> (f32, f32) {
        self.transformation_inverse.mult((x as f32, y as f32))
    }

    pub fn set_pixel(&mut self, x: f32, y: f32, col: u32) {
        let (tx, ty) = self.transform_point(x, y);

        self.set_pixel_direct(tx, ty, col)
    }

    pub fn set_pixel_direct(&mut self, x: i32, y: i32, color: u32) {
        if x < 0 || y < 0 || x >= self.width() || y >= self.height() {
            return;
        }

        let col: u32;
        if self.overlay {
            if (x - self.overlay_grid_x) % self.overlay_grid_d == 0 || (y - self.overlay_grid_y) % self.overlay_grid_d == 0 {
                col = self.overlay_grid_c;
            } else {
                col = color;
            }
        } else {
            col = color;
        }

        if self.buffer_enabled {
            let index = (x * self.height() + y) as usize;
            if self.buffer_value.get(index) != col {
                self.buffer_value.set(index, col);

                self.buffer_dirty[(x / 16) as usize][(y / 16) as usize] = true;
            }
        } else {
            self.layer1.print_point_color_at(x as usize, y as usize, Color::from_hex(col));
        }
    }

    pub fn clear(&mut self, col: u32) {
        for y in 0..lcd::HEIGHT {
            for x in 0..lcd::WIDTH {
                self.set_pixel_direct(x as i32, y as i32, col);
            }
        }
    }

    pub fn flush(&mut self, force: bool) {
        if !self.buffer_enabled {
            return;
        }

        for sy in 0..17 {
            for sx in 0..30 {
                if force || self.buffer_dirty[sx][sy] {
                    if DEBUG_REPAINT {
                        for x in sx * 16..(sx * 16 + 16) {
                            for y in sy * 16..(sy * 16 + 16) {
                                self.layer1.print_point_color_at(x, y, Color::from_hex(0x80_80_FF));
                            }
                        }
                        stm32f7_discovery::system_clock::wait_ms(8);
                    }

                    for x in sx * 16..(sx * 16 + 16) {
                        for y in sy * 16..(sy * 16 + 16) {
                            let index = x * lcd::HEIGHT + y;
                            self.layer1.print_point_color_at(x, y, Color::from_hex(self.buffer_value.get(index) & 0xFF_FF_FF));
                        }
                    }

                    self.buffer_dirty[sx][sy] = false;
                }
            }
        }

        /*
        for y in 0..lcd::HEIGHT {
        for x in 0..lcd::WIDTH {
        self.layer1.print_point_color_at(x as usize, y as usize, 0xFF_00_88);
        }
        }*/
    }

    pub fn transform(&mut self, matrix: [[f32; 3]; 3]) {
        //println!("Before:");
        //self.print_matrix(self.transformation);
        //println!("With:");
        //self.print_matrix(matrix);
        self.transformation = self.transformation.matrix_mult(matrix);
        self.transformation_inverse = self.transformation.invert();
        //println!("After:");
        //self.print_matrix(self.transformation);
        //println!("");
    }

    //    fn print_matrix(&self, m: [[i32; 3]; 3]) {
    //        println!("  [{}; {}; {}]", m[0][0], m[0][1], m[0][2]);
    //        println!("  [{}; {}; {}]", m[1][0], m[1][1], m[1][2]);
    //        println!("  [{}; {}; {}]", m[2][0], m[2][1], m[2][2]);
    //    }

    pub fn transform_offset(&mut self, ox: f32, oy: f32) {
        self.transform([[1.0, 0.0, ox], [0.0, 1.0, oy], [0.0, 0.0, 1.0]]);
    }

    pub fn transform_scale(&mut self, sx: f32, sy: f32) {
        self.transform([[sx, 0.0, 0.0], [0.0, sy, 0.0], [0.0, 0.0, 1.0]]);
    }

    pub fn transform_rotate_90_cw(&mut self) {
        // clockwise
        self.transform([[0.0, -1.0, 0.0], [1.0, 0.0, 0.0], [0.0, 0.0, 1.0]]);
    }

    pub fn transform_rotate_90_ccw(&mut self) {
        // clockwise
        self.transform([[0.0, 1.0, 0.0], [-1.0, 0.0, 0.0], [0.0, 0.0, 1.0]]);
    }

    fn vertical_line_direct(&mut self, y_start: i32, y_end: i32, x: i32, col: u32) {
        if y_end < y_start {
            for y in y_end + 1..=y_start {
                self.set_pixel_direct(x, y, col);
            }
        } else {
            for y in y_start..y_end {
                self.set_pixel_direct(x, y, col);
            }
        }
    }

    fn horizontal_line_direct(&mut self, x_start: i32, x_end: i32, y: i32, col: u32) {
        if x_end < x_start {
            for x in x_end + 1..=x_start {
                self.set_pixel_direct(x, y, col);
            }
        } else {
            for x in x_start..x_end {
                self.set_pixel_direct(x, y, col);
            }
        }
    }

    pub fn fill_image_rect(&mut self, x: f32, y: f32, width: f32, height: f32, mask: &[u64; 48], col0: u32, col1: u32) {
        let p0 = self.transform_point(x, y);
        let p1 = self.transform_point(x + width, y + height);

        for px in p0.0..p1.0 {
            for py in p0.1..p1.1 {
                let my = (47 - (py - p0.1)) as usize;
                let mx = (47 - (px - p0.0)) as usize;
                self.set_pixel_direct(px, py, if mask[mx] & (1 << my) != 0 { col1 } else { col0 });
            }
        }
    }

    pub fn fill_image_rect2(&mut self, x: f32, y: f32, width: f32, height: f32, mask: &[u64; 64], col0: u32, col1: u32, yoff: i32) {
        let p0 = self.transform_point(x, y);
        let p1 = self.transform_point(x + width, y + height);

        for px in p0.0..p1.0 {
            for py in p0.1..p1.1 {
                if (px - p0.0) < yoff + 64 && (px - p0.0) > yoff {
                    let my = (47 - (py - p0.1)) as usize;
                    let mx = (63 - ((px - p0.0) - yoff)) as usize;
                    self.set_pixel_direct(px, py, if mask[mx] & (1 << my) != 0 { col1 } else { col0 });
                } else {
                    self.set_pixel_direct(px, py, col1);
                }
            }
        }
    }

    pub fn fill_rect(&mut self, x: f32, y: f32, width: f32, height: f32, col: u32) {
        let p0 = self.transform_point(x, y);
        let p1 = self.transform_point(x + width, y + height);

        for px in p0.0..p1.0 {
            for py in p0.1..p1.1 {
                self.set_pixel_direct(px, py, col);
            }
        }
    }

    pub fn exec_on_rect<F>(&mut self, x: f32, y: f32, width: f32, height: f32, mut func: F)
    where
        F: FnMut(&mut Self, i32, i32, i32, i32),
    {
        let p0 = self.transform_point(x, y);
        let p1 = self.transform_point(x + width, y + height);

        for px in p0.0..p1.0 {
            for py in p0.1..p1.1 {
                func(self, px, py, px - p0.0, py - p0.1);
            }
        }
    }

    pub fn draw_rect(&mut self, x: f32, y: f32, width: f32, height: f32, col: u32) {
        self.draw_line(x, y, x + width, y, col);
        self.draw_line(x, y, x, y + height, col);
        self.draw_line(x, y + height, x + width, y + height, col);
        self.draw_line(x + width, y, x + width, y + height, col);
    }

    // https://de.wikipedia.org/wiki/Bresenham-Algorithmus#Kompakte_Variante
    fn exec_on_line<F>(&mut self, x_start: f32, y_start: f32, x_end: f32, y_end: f32, mut func: F)
    where
        F: FnMut(&mut Self, i32, i32),
    {
        let p0 = self.transform_point(x_start, y_start);
        let p1 = self.transform_point(x_end, y_end);

        //println!("line: ({}|{}) -> ({}|{})", p0.0, p0.1, p1.0, p1.1);
        //println!("    @ ({}|{}) -> ({}|{})", x_start, y_start, x_end, y_end);

        let mut x0 = p0.0;
        let mut y0 = p0.1;
        let x1 = p1.0;
        let y1 = p1.1;

        let dx: i32 = (x1 - x0).abs();
        let sx: i32 = if x0 < x1 { 1 } else { -1 };
        let dy: i32 = -(y1 - y0).abs();
        let sy: i32 = if y0 < y1 { 1 } else { -1 };

        let mut err: i32 = dx + dy;

        loop {
            func(self, x0, y0);
            if x0 == x1 && y0 == y1 {
                break;
            }
            let e2 = 2 * err;
            if e2 > dy {
                err += dy;
                x0 += sx;
            }
            if e2 < dx {
                err += dx;
                y0 += sy;
            }
        }
    }

    pub fn draw_line(&mut self, x_start: f32, y_start: f32, x_end: f32, y_end: f32, col: u32) {
        self.exec_on_line(x_start, y_start, x_end, y_end, |g, x, y| g.set_pixel_direct(x, y, col));
    }

    pub fn draw_dot(&mut self, x: f32, y: f32, width: f32, col: u32) {
        self.fill_rect(x - width / 2.0, y - width / 2.0, width, width, col);
    }

    pub fn width(&self) -> i32 {
        lcd::WIDTH as i32
    }

    pub fn height(&self) -> i32 {
        lcd::HEIGHT as i32
    }

    pub fn transformation_push(&mut self) {
        if self.transformation_backup_index == self.transformation_backup.len() {
            return;
        }

        self.transformation_backup[self.transformation_backup_index] = Some((self.transformation, self.transformation_inverse));
        self.transformation_backup_index += 1;
    }

    pub fn transformation_pop(&mut self) {
        if self.transformation_backup_index == 0 {
            return;
        }

        if let Some(f) = self.transformation_backup[self.transformation_backup_index - 1].take() {
            self.transformation = f.0;
            self.transformation_inverse = f.1;
        }

        self.transformation_backup_index -= 1;
    }

    pub fn pixel_width(&self) -> f32 {
        math::fabsf(self.inverse_transform_point(1, 1).0 - self.inverse_transform_point(0, 0).0)
    }

    pub fn pixel_height(&self) -> f32 {
        math::fabsf(self.inverse_transform_point(1, 1).1 - self.inverse_transform_point(0, 0).1)
    }

    pub fn draw_function_joined<F>(&mut self, start_x: f32, end_x: f32, func: F, col: u32)
    where
        F: Fn(f32) -> f32,
    {
        let mut last_x: Option<f32> = None;
        let mut last_y: Option<f32> = None;

        let dx = self.pixel_width() * 2.0;

        let steps = math::ceilf((end_x - start_x) / dx) as i32;

        for step in 0..steps {
            let x = start_x + (step as f32) * dx;
            let y = func(x);

            //println!("{} ; {} = ({}|{}) [[ {}", dx, step, x, y, steps);

            if last_x.is_some() {
                self.draw_line(last_x.unwrap(), last_y.unwrap(), x, y, col);
            }
            last_x = Some(x);
            last_y = Some(y);
        }
    }

    pub fn draw_function<F>(&mut self, start_x: f32, end_x: f32, func: F, col: u32)
    where
        F: Fn(f32) -> f32,
    {
        let dx = self.pixel_width();
        let steps = math::ceilf((end_x - start_x) / dx) as i32;

        for step in 0..steps {
            let x = start_x + (step as f32) * dx;
            let y = func(x);

            self.set_pixel(x, y, col)
        }
    }

    pub fn fill_function<F>(&mut self, start_x: f32, end_x: f32, func: F, col: u32)
    where
        F: Fn(f32) -> f32,
    {
        let dx = self.pixel_width() * 2.0;

        for step in 0..(math::ceilf((end_x - start_x) / dx) as i32) {
            let x = start_x + (step as f32) * dx;
            let y = func(x);

            self.draw_line(x, y, x, 0.0, col);
        }
    }

    pub fn fill_function_mirror<F>(&mut self, start_x: f32, end_x: f32, func: F, col: u32)
    where
        F: Fn(f32) -> f32,
    {
        let dx = self.pixel_width() * 2.0;

        for step in 0..(math::ceilf((end_x - start_x) / dx) as i32) {
            let x = start_x + (step as f32) * dx;
            let y = func(x);

            self.draw_line(x, y, x, -y, col);
        }
    }

    pub fn enable_grid(&mut self, ox: i32, oy: i32, dist: i32, col: u32) {
        self.overlay = true;
        self.overlay_grid_x = ox;
        self.overlay_grid_y = oy;
        self.overlay_grid_d = dist;
        self.overlay_grid_c = col;
    }

    pub fn disable_grid(&mut self) {
        self.overlay = false;
    }

    pub fn convert_touches(&self, input: [Option<(u16, u16)>; 5]) -> [Option<(f32, f32)>; 5] {
        let mut output = [None; 5];

        for i in 0..5 {
            output[i] = match input[i] {
                None => None,
                Some(p) => Some(self.inverse_transform_point(i32::from(p.0), i32::from(p.1))),
            }
        }

        output
    }
}
