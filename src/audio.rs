use crate::consts::*;
use crate::hardware::*;
use crate::rambuffer::*;
use crate::util::*;

pub const BITRATE: u32 = 16_384;

const IDX_AMP: usize = 0;
const IDX_PLAYBACK: usize = 1;

static mut BUFFER_DATA: RamBuffer = RamBuffer::new(0xC020_0000, (BITRATE * 4) as usize); // 0xC020_0000 .. 0xC021_0000   (  circular buffer (1 second)  )
static mut BUFFER_META: RamBuffer = RamBuffer::new(0xC030_0000, 2 * 4); // 0xC030_0000 .. 0xC030_0008   <amp, playback_pos>

static mut AUDIO_INIT: bool = false;

#[derive(PartialEq, Copy, Clone)]
pub struct AudioGenerator {
    pub value: u8,
}

#[derive(PartialEq, Copy, Clone)]
pub enum SingleAudioGenerator {
    Zero = 0b00_00_00_00,
    Square = 0b00_00_00_01,
    Sin = 0b00_00_00_10,
    Saw = 0b00_00_01_00,
    Sin3 = 0b00_00_10_00,
    Noise = 0b00_01_00_00,
    Triangle = 0b00_10_00_00,
}

impl AudioGenerator {
    pub fn new0() -> Self {
        Self { value: 0 }
    }

    pub fn new1(sag: SingleAudioGenerator) -> Self {
        Self { value: sag as u8 }
    }

    pub fn calc(&self, x: u32, freq: f32) -> f32 {
        let mut v = 0.0;
        let mut c = 0;
        //if self.value & (SingleAudioGenerator::Zero as u8) != 0 {
        //    v += SingleAudioGenerator::to_function(&SingleAudioGenerator::Zero)(x, freq);
        //    c += 1;
        //}
        if self.value & (SingleAudioGenerator::Square as u8) != 0 {
            v += SingleAudioGenerator::to_function(&SingleAudioGenerator::Square)(x, freq);
            c += 1;
        }
        if self.value & (SingleAudioGenerator::Sin as u8) != 0 {
            v += SingleAudioGenerator::to_function(&SingleAudioGenerator::Sin)(x, freq);
            c += 1;
        }
        if self.value & (SingleAudioGenerator::Saw as u8) != 0 {
            v += SingleAudioGenerator::to_function(&SingleAudioGenerator::Saw)(x, freq);
            c += 1;
        }
        if self.value & (SingleAudioGenerator::Sin3 as u8) != 0 {
            v += SingleAudioGenerator::to_function(&SingleAudioGenerator::Sin3)(x, freq);
            c += 1;
        }
        if self.value & (SingleAudioGenerator::Noise as u8) != 0 {
            v += SingleAudioGenerator::to_function(&SingleAudioGenerator::Noise)(x, freq);
            c += 1;
        }
        if self.value & (SingleAudioGenerator::Triangle as u8) != 0 {
            v += SingleAudioGenerator::to_function(&SingleAudioGenerator::Triangle)(x, freq);
            c += 1;
        }
        if c == 0 {
            return 1.0;
        }
        return v / (c as f32);
    }

    pub fn contains(&self, g: SingleAudioGenerator) -> bool {
        self.value & (g as u8) != 0
    }

    pub fn toggle(&self, g: SingleAudioGenerator) -> AudioGenerator {
        AudioGenerator { value: self.value ^ (g as u8) }
    }

    pub fn single(&self) -> SingleAudioGenerator {
        if self.contains(SingleAudioGenerator::Square) {
            SingleAudioGenerator::Square
        } else if self.contains(SingleAudioGenerator::Sin) {
            SingleAudioGenerator::Sin
        } else if self.contains(SingleAudioGenerator::Saw) {
            SingleAudioGenerator::Saw
        } else if self.contains(SingleAudioGenerator::Sin3) {
            SingleAudioGenerator::Sin3
        } else if self.contains(SingleAudioGenerator::Noise) {
            SingleAudioGenerator::Noise
        } else if self.contains(SingleAudioGenerator::Triangle) {
            SingleAudioGenerator::Triangle
        } else {
            SingleAudioGenerator::Zero
        }
    }
}

impl SingleAudioGenerator {
    pub fn to_function(generator: &SingleAudioGenerator) -> &Fn(u32, f32) -> f32 {
        match generator {
            SingleAudioGenerator::Zero => &AudioMan::zero,
            SingleAudioGenerator::Square => &AudioMan::square,
            SingleAudioGenerator::Sin => &AudioMan::sine,
            SingleAudioGenerator::Saw => &AudioMan::saw,
            SingleAudioGenerator::Sin3 => &AudioMan::sine3,
            SingleAudioGenerator::Noise => &AudioMan::noise,
            SingleAudioGenerator::Triangle => &AudioMan::triangle,
        }
    }
}

#[derive(Default)]
pub struct AudioMan {
    pub square_state: u8,

    pub buffer_generate_position: u32,
    pub buffer_needs_refresh: bool,
    pub buffer_semi_refresh: bool,
}

impl AudioMan {
    pub fn new() -> Self {
        let r = AudioMan {
            square_state: 0,

            buffer_generate_position: 0,
            buffer_needs_refresh: true,
            buffer_semi_refresh: false,
        };

        AudioMan::set_amp(10_000);
        AudioMan::set_index_playback(0);

        unsafe {
            AUDIO_INIT = true;
        }

        r
    }

    pub fn zero(_x: u32, _freq: f32) -> f32 {
        1.0
    }

    pub fn one(_x: u32, _freq: f32) -> f32 {
        2.0
    }

    pub fn sine(x: u32, freq: f32) -> f32 {
        1. + Util::sin(2. * PI * freq * x as f32 / CLOCKSPEED)
    }

    pub fn sine3(x: u32, freq: f32) -> f32 {
        1. + Util::sin3(2. * PI * freq * x as f32 / CLOCKSPEED)
    }

    pub fn saw(x: u32, freq: f32) -> f32 {
        (((x as f32 * freq) / (CLOCKSPEED as f32)) % 1.0) * 2.0
    }

    pub fn triangle(x: u32, freq: f32) -> f32 {
        let r = ((x as f32 * 2.0 * freq) / (CLOCKSPEED as f32)) % 2.0;
        if r > 1.0 {
            (2.0 - r) * 2.0
        } else {
            r * 2.0
        }
    }

    pub fn noise(_x: u32, _freq: f32) -> f32 {
        Util::rand() * 2.0
    }

    pub fn peak(x: u32, freq: f32) -> f32 {
        Util::peak((x as f32 * 2.0 * freq) / (CLOCKSPEED as f32))
    }

    pub fn square(x: u32, freq: f32) -> f32 {
        if (((x as f32 * 2.0 * freq) / (CLOCKSPEED as f32)) as u32) % 2 == 0 {
            2.0
        } else {
            0.0
        }
    }

    pub fn play_tick(recorded: u32, num_writes: u32) -> u32 {
        unsafe {
            if !AUDIO_INIT {
                return 0;
            }
        }
        let playbackpos_r = AudioMan::get_index_playback() + 1;
        let playbackpos = playbackpos_r % BITRATE;
        let mut value;

        unsafe {
            value = BUFFER_DATA.get(playbackpos as usize);
            value = value / num_writes + (num_writes - 1) * recorded / num_writes;
            //value = (value+recorded) - (value*recorded/(1+Util::max_u32(value, recorded)));
            if let Some(sai2) = &crate::hardware::GLOB_SAI2 {
                if sai2.asr.read().freq().bit_is_set() {
                    // fifo_request_flag
                    sai2.adr.write(|w| {
                        w.data().bits(value);
                        w
                    });
                }
            }
        }
        AudioMan::set_index_playback(playbackpos_r);
        return value;
    }

    pub fn fill_buffer<F: Fn(f32, u32, f32) -> f32>(&mut self, freq: f32, func: F) {
        let playback = AudioMan::get_index_playback();
        let amp = AudioMan::get_amp() as f32;

        if self.buffer_generate_position < playback {
            self.buffer_generate_position = playback;
        }

        if self.buffer_needs_refresh {
            self.buffer_generate_position = Util::min_u32(playback + 1024, self.buffer_generate_position); // = 64ms

            for i in self.buffer_generate_position..(self.buffer_generate_position + 4096) {
                // 128ms
                unsafe {
                    BUFFER_DATA.set(i as usize, (func(amp, i, freq)) as u32);
                }
            }
            self.buffer_generate_position += 4096;

            self.buffer_needs_refresh = false;
            self.buffer_semi_refresh = true;
            return;
        } else if self.buffer_semi_refresh {
            for i in self.buffer_generate_position..(self.buffer_generate_position + 4096) {
                // 256ms
                unsafe {
                    BUFFER_DATA.set(i as usize, (func(amp, i, freq)) as u32);
                }
            }
            self.buffer_generate_position += 4096;

            if self.buffer_generate_position - BITRATE >= playback {
                self.buffer_semi_refresh = false;
            }
            return;
        } else {
            if self.buffer_generate_position >= playback + BITRATE {
                return;
            }

            for i in self.buffer_generate_position.. {
                if i - BITRATE >= playback {
                    self.buffer_generate_position = i;
                    return;
                }

                unsafe {
                    BUFFER_DATA.set(i as usize, (func(amp, i, freq)) as u32);
                }
            }
        }
    }

    pub fn get_index_playback() -> u32 {
        unsafe { BUFFER_META.get(IDX_PLAYBACK) }
    }

    pub fn set_index_playback(val: u32) {
        unsafe {
            BUFFER_META.set(IDX_PLAYBACK, val);
        }
    }

    pub fn get_amp() -> u32 {
        unsafe { BUFFER_META.get(IDX_AMP) }
    }

    pub fn set_amp(val: u32) {
        unsafe {
            BUFFER_META.set(IDX_AMP, val);
        }
    }

    pub fn force_refresh(&mut self) {
        self.buffer_needs_refresh = true;
        self.buffer_semi_refresh = false;
    }
}
