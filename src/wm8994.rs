use stm32f7::stm32f7x6::*;
use stm32f7_discovery::*;

type I2cError = Result<(), i2c::Error>;
type I2c3Connection<'a> = i2c::I2cConnection<'a, I2C3, u16>;

pub enum OutputInputDevice {
    OutputDeviceSpeaker = 0x01,
    OutputDeviceHeadphone = 0x02,
    OutputDeviceBoth = 0x03,
    OutputDeviceAuto = 0x04,
    InputDeviceDigitalMicrophone1 = 0x0100,
    InputDeviceDigitalMicrophone2 = 0x0200,
    InputDeviceDigitalMic1Mic2 = 0x0300,
    InputDeviceInputLine1 = 0x0400,
}

impl OutputInputDevice {
    pub fn from_value(value: u16) -> OutputInputDevice {
        match value {
            0x0001 => OutputInputDevice::OutputDeviceSpeaker,
            0x0002 => OutputInputDevice::OutputDeviceHeadphone,
            0x0003 => OutputInputDevice::OutputDeviceBoth,
            0x0004 => OutputInputDevice::OutputDeviceAuto,
            0x0100 => OutputInputDevice::InputDeviceDigitalMicrophone1,
            0x0200 => OutputInputDevice::InputDeviceDigitalMicrophone2,
            0x0300 => OutputInputDevice::InputDeviceDigitalMic1Mic2,
            0x0400 => OutputInputDevice::InputDeviceInputLine1,
            _ => OutputInputDevice::OutputDeviceAuto,
        }
    }
}

pub enum Frequency {
    AudioFrequency192K = 192_000,
    AudioFrequency96K = 96_000,
    AudioFrequency48K = 48_000,
    AudioFrequency44K = 44_100,
    AudioFrequency32K = 32_000,
    AudioFrequency22K = 22_050,
    AudioFrequency16K = 16_000,
    AudioFrequency11K = 11_025,
    AudioFrequency8K = 8_000,
}

impl Frequency {
    pub fn from_value(value: u32) -> Frequency {
        match value {
            192_000 => Frequency::AudioFrequency192K,
            96_000 => Frequency::AudioFrequency96K,
            48_000 => Frequency::AudioFrequency48K,
            44_100 => Frequency::AudioFrequency44K,
            32_000 => Frequency::AudioFrequency32K,
            22_050 => Frequency::AudioFrequency22K,
            16_000 => Frequency::AudioFrequency16K,
            11_025 => Frequency::AudioFrequency11K,
            80_00 => Frequency::AudioFrequency8K,
            _ => Frequency::AudioFrequency16K,
        }
    }
}

pub struct WM8994 {
    ic2_address: i2c::Address,
    frequency: Frequency,
    output_enabled: bool,
    input_enabled: bool,
    output_device: u16,
    input_device: u16,
}

impl WM8994 {
    pub fn new(frequency: Frequency) -> Self {
        Self {
            ic2_address: i2c::Address::bits_7(0b001_1010),
            frequency,
            output_enabled: false,
            input_enabled: false,
            output_device: 0,
            input_device: 0,
        }
    }

    fn reset_device(conn: &mut I2c3Connection) -> I2cError {
        conn.write(0, 0)
    }

    fn path_configuration_output_speaker(conn: &mut I2c3Connection) -> I2cError {
        /* Enable DAC1 (Left), Enable DAC1 (Right), Disable DAC2 (Left), Disable DAC2 (Right)*/
        conn.write(0x05, 0x0C0C)?;
        /* Enable the AIF1 Timeslot 0 (Left) to DAC 1 (Left) mixer path */
        conn.write(0x601, 0x0000)?;
        /* Enable the AIF1 Timeslot 0 (Right) to DAC 1 (Right) mixer path */
        conn.write(0x602, 0x0000)?;
        /* Disable the AIF1 Timeslot 1 (Left) to DAC 2 (Left) mixer path */
        conn.write(0x604, 0x0002)?;
        /* Disable the AIF1 Timeslot 1 (Right) to DAC 2 (Right) mixer path */
        conn.write(0x605, 0x0002)?;
        Ok(())
    }

    fn path_configuration_output_headphone(conn: &mut I2c3Connection) -> I2cError {
        /* Disable DAC1 (Left), Disable DAC1 (Right), Enable DAC2 (Left), Enable DAC2 (Right)*/
        conn.write(0x05, 0x0303)?;
        /* Enable the AIF1 Timeslot 0 (Left) to DAC 1 (Left) mixer path */
        conn.write(0x601, 0x0001)?;
        /* Enable the AIF1 Timeslot 0 (Right) to DAC 1 (Right) mixer path */
        conn.write(0x602, 0x0001)?;
        /* Disable the AIF1 Timeslot 1 (Left) to DAC 2 (Left) mixer path */
        conn.write(0x604, 0x0000)?;
        /* Disable the AIF1 Timeslot 1 (Right) to DAC 2 (Right) mixer path */
        conn.write(0x605, 0x0000)?;
        Ok(())
    }

    fn path_configuration_output_both(conn: &mut I2c3Connection, with_input: bool) -> I2cError {
        if with_input {
            /* Enable DAC1 (Left), Enable DAC1 (Right), also Enable DAC2 (Left), Enable DAC2 (Right)*/
            conn.write(0x05, 0x0303 | 0x0C0C)?;
            /* Enable the AIF1 Timeslot 0 (Left) to DAC 1 (Left) mixer path
             * Enable the AIF1 Timeslot 1 (Left) to DAC 1 (Left) mixer path */
            conn.write(0x601, 0x0003)?;
            /* Enable the AIF1 Timeslot 0 (Right) to DAC 1 (Right) mixer path
             * Enable the AIF1 Timeslot 1 (Right) to DAC 1 (Right) mixer path */
            conn.write(0x602, 0x0003)?;
            /* Enable the AIF1 Timeslot 0 (Left) to DAC 2 (Left) mixer path
             * Enable the AIF1 Timeslot 1 (Left) to DAC 2 (Left) mixer path  */
            conn.write(0x604, 0x0003)?;
            /* Enable the AIF1 Timeslot 0 (Right) to DAC 2 (Right) mixer path
             * Enable the AIF1 Timeslot 1 (Right) to DAC 2 (Right) mixer path */
            conn.write(0x605, 0x0003)?;
        } else {
            /* Enable DAC1 (Left), Enable DAC1 (Right), also Enable DAC2 (Left), Enable DAC2 (Right)*/
            conn.write(0x05, 0x0303 | 0x0C0C)?;
            /* Enable the AIF1 Timeslot 0 (Left) to DAC 1 (Left) mixer path */
            conn.write(0x601, 0x0001)?;
            /* Enable the AIF1 Timeslot 0 (Right) to DAC 1 (Right) mixer path */
            conn.write(0x602, 0x0001)?;
            /* Enable the AIF1 Timeslot 1 (Left) to DAC 2 (Left) mixer path */
            conn.write(0x604, 0x0002)?;
            /* Enable the AIF1 Timeslot 1 (Right) to DAC 2 (Right) mixer path */
            conn.write(0x605, 0x0002)?;
        }
        Ok(())
    }

    fn path_configuration_output_auto(conn: &mut I2c3Connection) -> I2cError {
        /* Disable DAC1 (Left), Disable DAC1 (Right), Enable DAC2 (Left), Enable DAC2 (Right)*/
        conn.write(0x05, 0x0303)?;
        /* Enable the AIF1 Timeslot 0 (Left) to DAC 1 (Left) mixer path */
        conn.write(0x601, 0x0001)?;
        /* Enable the AIF1 Timeslot 0 (Right) to DAC 1 (Right) mixer path */
        conn.write(0x602, 0x0001)?;
        /* Disable the AIF1 Timeslot 1 (Left) to DAC 2 (Left) mixer path */
        conn.write(0x604, 0x0000)?;
        /* Disable the AIF1 Timeslot 1 (Right) to DAC 2 (Right) mixer path */
        conn.write(0x605, 0x0000)?;
        Ok(())
    }

    fn path_configuration_input_mic1(conn: &mut I2c3Connection) -> I2cError {
        /* Disable DAC1 (Left), Disable DAC1 (Right), Enable DAC2 (Left), Enable DAC2 (Right)*/
        conn.write(0x05, 0x0303)?;
        /* Enable the AIF1 Timeslot 0 (Left) to DAC 1 (Left) mixer path */
        conn.write(0x601, 0x0001)?;
        /* Enable the AIF1 Timeslot 0 (Right) to DAC 1 (Right) mixer path */
        conn.write(0x602, 0x0001)?;
        /* Disable the AIF1 Timeslot 1 (Left) to DAC 2 (Left) mixer path */
        conn.write(0x604, 0x0000)?;
        /* Disable the AIF1 Timeslot 1 (Right) to DAC 2 (Right) mixer path */
        conn.write(0x605, 0x0000)?;
        Ok(())
    }

    fn path_configuration_input_mic2(conn: &mut I2c3Connection) -> I2cError {
        /* Enable AIF1ADC2 (Left), Enable AIF1ADC2 (Right)
         * Enable DMICDAT2 (Left), Enable DMICDAT2 (Right)
         * Enable Left ADC, Enable Right ADC */
        conn.write(0x04, 0x0C30)?;
        /* Enable AIF1 DRC2 Signal Detect & DRC in AIF1ADC2 Left/Right Timeslot 1 */
        conn.write(0x450, 0x00DB)?;
        /* Disable IN1L, IN1R, IN2L, IN2R, Enable Thermal sensor & shutdown */
        conn.write(0x02, 0x6000)?;
        /* Enable the DMIC2(Left) to AIF1 Timeslot 1 (Left) mixer path */
        conn.write(0x608, 0x0002)?;
        /* Enable the DMIC2(Right) to AIF1 Timeslot 1 (Right) mixer path */
        conn.write(0x609, 0x0002)?;
        /* GPIO1 pin configuration GP1_DIR = output, GP1_FN = AIF1 DRC2 signal detect */
        conn.write(0x700, 0x000E)?;
        Ok(())
    }

    fn path_configuration_input_mic_both(conn: &mut I2c3Connection) -> I2cError {
        /* Enable AIF1ADC1 (Left), Enable AIF1ADC1 (Right)
         * Enable DMICDAT1 (Left), Enable DMICDAT1 (Right)
         * Enable Left ADC, Enable Right ADC */
        conn.write(0x04, 0x0F3C)?;
        /* Enable AIF1 DRC2 Signal Detect & DRC in AIF1ADC2 Left/Right Timeslot 1 */
        conn.write(0x450, 0x00DB)?;
        /* Enable AIF1 DRC2 Signal Detect & DRC in AIF1ADC1 Left/Right Timeslot 0 */
        conn.write(0x440, 0x00DB)?;
        /* Disable IN1L, IN1R, Enable IN2L, IN2R, Thermal sensor & shutdown */
        conn.write(0x02, 0x63A0)?;
        /* Enable the DMIC2(Left) to AIF1 Timeslot 0 (Left) mixer path */
        conn.write(0x606, 0x0002)?;
        /* Enable the DMIC2(Right) to AIF1 Timeslot 0 (Right) mixer path */
        conn.write(0x607, 0x0002)?;
        /* Enable the DMIC2(Left) to AIF1 Timeslot 1 (Left) mixer path */
        conn.write(0x608, 0x0002)?;
        /* Enable the DMIC2(Right) to AIF1 Timeslot 1 (Right) mixer path */
        conn.write(0x609, 0x0002)?;
        /* GPIO1 pin configuration GP1_DIR = output, GP1_FN = AIF1 DRC1 signal detect */
        conn.write(0x700, 0x000D)?;
        Ok(())
    }

    fn path_configuration_input_line1(conn: &mut I2c3Connection) -> I2cError {
        /* IN1LN_TO_IN1L, IN1LP_TO_VMID, IN1RN_TO_IN1R, IN1RP_TO_VMID */
        conn.write(0x28, 0x0011)?;
        /* Disable mute on IN1L_TO_MIXINL and +30dB on IN1L PGA output */
        conn.write(0x29, 0x0035)?;
        /* Disable mute on IN1R_TO_MIXINL, Gain = +30dB */
        conn.write(0x2A, 0x0035)?;
        /* Enable AIF1ADC1 (Left), Enable AIF1ADC1 (Right) * Enable Left ADC, Enable Right ADC */
        conn.write(0x04, 0x0303)?;
        /* Enable AIF1 DRC1 Signal Detect & DRC in AIF1ADC1 Left/Right Timeslot 0 */
        conn.write(0x440, 0x00DB)?;
        /* Enable IN1L and IN1R, Disable IN2L and IN2R, Enable Thermal sensor & shutdown */
        conn.write(0x02, 0x6350)?;
        /* Enable the ADCL(Left) to AIF1 Timeslot 0 (Left) mixer path */
        conn.write(0x606, 0x0002)?;
        /* Enable the ADCR(Right) to AIF1 Timeslot 0 (Right) mixer path */
        conn.write(0x607, 0x0002)?;
        /* GPIO1 pin configuration GP1_DIR = output, GP1_FN = AIF1 DRC1 signal detect */
        conn.write(0x700, 0x000D)?;
        Ok(())
    }

    fn clock_configuration(&mut self, conn: &mut I2c3Connection) -> I2cError {
        match self.frequency {
            Frequency::AudioFrequency8K => conn.write(0x210, 0x0003),  /* AIF1 Sample Rate = 8 (KHz), ratio=256 */
            Frequency::AudioFrequency16K => conn.write(0x210, 0x0033), /* AIF1 Sample Rate = 16 (KHz), ratio=256 */
            Frequency::AudioFrequency32K => conn.write(0x210, 0x0063), /* AIF1 Sample Rate = 32 (KHz), ratio=256 */
            Frequency::AudioFrequency48K => conn.write(0x210, 0x0083), /* AIF1 Sample Rate = 48 (KHz), ratio=256 */
            Frequency::AudioFrequency96K => conn.write(0x210, 0x00A3), /* AIF1 Sample Rate = 96 (KHz), ratio=256 */
            Frequency::AudioFrequency11K => conn.write(0x210, 0x0013), /* AIF1 Sample Rate = 11.025 (KHz), ratio=256 */
            Frequency::AudioFrequency22K => conn.write(0x210, 0x0043), /* AIF1 Sample Rate = 22.050 (KHz), ratio=256 */
            Frequency::AudioFrequency44K => conn.write(0x210, 0x0073), /* AIF1 Sample Rate = 44.1 (KHz), ratio=256 */
            _ => conn.write(0x210, 0x0083),                            /* AIF1 Sample Rate = 48 (KHz), ratio=256 */
        }?;
        if self.input_device == OutputInputDevice::InputDeviceDigitalMic1Mic2 as u16 {
            /* AIF1 Word Length = 16-bits, AIF1 Format = DSP mode */
            conn.write(0x300, 0x4018)?;
        } else {
            /* AIF1 Word Length = 16-bits, AIF1 Format = I2S (Default Register Value) */
            conn.write(0x300, 0x4010)?;
        }
        /* slave mode */
        conn.write(0x302, 0x0000)?;
        /* Enable the DSP processing clock for AIF1, Enable the core clock */
        conn.write(0x208, 0x000A)?;
        /* Enable AIF1 Clock, AIF1 Clock Source = MCLK1 pin */
        conn.write(0x200, 0x0001)?;
        Ok(())
    }

    fn set_mute(&mut self, conn: &mut I2c3Connection, mute: bool) -> I2cError {
        if self.output_enabled {
            if mute {
                /* Soft Mute the AIF1 Timeslot 0 DAC1 path L&R */
                conn.write(0x420, 0x0200)?;
                /* Soft Mute the AIF1 Timeslot 1 DAC2 path L&R */
                conn.write(0x422, 0x0200)?;
            } else {
                /* Unmute the AIF1 Timeslot 0 DAC1 path L&R */
                conn.write(0x420, 0x0010)?;
                /* Unmute the AIF1 Timeslot 1 DAC2 path L&R */
                conn.write(0x422, 0x0010)?;
            }
        }
        Ok(())
    }

    fn set_volume(&mut self, conn: &mut I2c3Connection, volume: u8) -> I2cError {
        /* Output volume */
        if self.output_enabled {
            let converted_volume = u16::from(if volume > 100 { 100 } else { volume * 63 / 100 });
            if converted_volume > 0x3E {
                /* Unmute audio codec */
                self.set_mute(conn, false)?;
                /* Left Headphone Volume */
                conn.write(0x1C, 0x3F | 0x140)?;
                /* Right Headphone Volume */
                conn.write(0x1D, 0x3F | 0x140)?;
                /* Left Speaker Volume */
                conn.write(0x26, 0x3F | 0x140)?;
                /* Right Speaker Volume */
                conn.write(0x27, 0x3F | 0x140)?;
            } else if volume == 0 {
                /* Mute audio codec */
                self.set_mute(conn, true)?;
            } else {
                /* Unmute audio codec */
                self.set_mute(conn, false)?;
                /* Left Headphone Volume */
                conn.write(0x1C, converted_volume | 0x140)?;
                /* Right Headphone Volume */
                conn.write(0x1D, converted_volume | 0x140)?;
                /* Left Speaker Volume */
                conn.write(0x26, converted_volume | 0x140)?;
                /* Right Speaker Volume */
                conn.write(0x27, converted_volume | 0x140)?;
            }
        }
        /* Input volume */
        if self.input_enabled {
            let converted_volume = u16::from(if volume >= 100 { 239 } else { volume * 240 / 100 });
            /* Left AIF1 ADC1 volume */
            conn.write(0x400, converted_volume | 0x100)?;
            /* Right AIF1 ADC1 volume */
            conn.write(0x401, converted_volume | 0x100)?;
            /* Left AIF1 ADC2 volume */
            conn.write(0x404, converted_volume | 0x100)?;
            /* Right AIF1 ADC2 volume */
            conn.write(0x405, converted_volume | 0x100)?;
        }
        Ok(())
    }

    fn cold_startup(&mut self, conn: &mut I2c3Connection) -> I2cError {
        if self.output_enabled {
            /* Audio output selected */
            if self.output_device == OutputInputDevice::OutputDeviceHeadphone as u16 {
                /* Select DAC1 (Left) to Left Headphone Output PGA (HPOUT1LVOL) path */
                conn.write(0x2D, 0x0100)?;
                /* Select DAC1 (Right) to Right Headphone Output PGA (HPOUT1RVOL) path */
                conn.write(0x2E, 0x0100)?;
                /* Startup sequence for Headphone */
                conn.write(0x110, 0x8100)?;
                /* Add Delay */
                system_clock::wait_ms(300);
                /* Soft un-Mute the AIF1 Timeslot 0 DAC1 path L&R */
                conn.write(0x420, 0x0000)?;
            }
            /* Analog Output Configuration */

            /* Enable SPKRVOL PGA, Enable SPKMIXR, Enable SPKLVOL PGA, Enable SPKMIXL */
            conn.write(0x03, 0x0300)?;
            /* Left Speaker Mixer Volume = 0dB */
            conn.write(0x22, 0x0000)?;
            /* Speaker output mode = Class D, Right Speaker Mixer Volume = 0dB ((0x23, 0x0100) = class AB)*/
            conn.write(0x23, 0x0000)?;
            /* Unmute DAC2 (Left) to Left Speaker Mixer (SPKMIXL) path,
             * Unmute DAC2 (Right) to Right Speaker Mixer (SPKMIXR) path */
            conn.write(0x36, 0x0300)?;
            /* Enable bias generator, Enable VMID, Enable SPKOUTL,
             *
             * Enable SPKOUTR */
            conn.write(0x01, 0x3003)?;

            /* Headphone/Speaker Enable */
            if self.input_device == OutputInputDevice::InputDeviceDigitalMic1Mic2 as u16 {
                /* Enable Class W, Class W Envelope Tracking = AIF1 Timeslots 0 and 1 */
                conn.write(0x51, 0x0205)?;
            } else {
                /* Enable Class W, Class W Envelope Tracking = AIF1 Timeslot 0 */
                conn.write(0x51, 0x0005)?;
            }
            /* Enable bias generator, Enable VMID,
             *
             * Enable HPOUT1 (Left) and Enable HPOUT1 (Right) input stages */
            /* idem for Speaker */
            let power_mgnt_reg_1 = 0x0303 | 0x3003;
            conn.write(0x01, power_mgnt_reg_1)?;

            /* Enable HPOUT1 (Left) and HPOUT1 (Right) intermediate stages */
            conn.write(0x60, 0x0022)?;
            /* Enable Charge Pump */
            conn.write(0x4C, 0x9F25)?;
            /* Add Delay */
            system_clock::wait_ms(15);
            /* Select DAC1 (Left) to Left Headphone Output PGA (HPOUT1LVOL) path */
            conn.write(0x2D, 0x0001)?;
            /* Select DAC1 (Right) to Right Headphone Output PGA (HPOUT1RVOL) path */
            conn.write(0x2E, 0x0001)?;
            /* Enable Left Output Mixer (MIXOUTL), Enable Right Output Mixer (MIXOUTR) */
            /* idem for SPKOUTL and SPKOUTR */
            conn.write(0x03, 0x0030 | 0x0300)?;
            /* Enable DC Servo and trigger start-up mode on left and right channels */
            conn.write(0x54, 0x0033)?;
            /* Add Delay */
            system_clock::wait_ms(257);
            /* Enable HPOUT1 (Left) and HPOUT1 (Right) intermediate and output stages.
             * Remove clamps */
            conn.write(0x60, 0x00EE)?;

            /* Unmutes */

            /* Unmute DAC 1 (Left) */
            conn.write(0x610, 0x00C0)?;
            /* Unmute DAC 1 (Right) */
            conn.write(0x611, 0x00C0)?;
            /* Unmute the AIF1 Timeslot 0 DAC path */
            conn.write(0x420, 0x0010)?;
            /* Unmute DAC 2 (Left) */
            conn.write(0x612, 0x00C0)?;
            /* Unmute DAC 2 (Right) */
            conn.write(0x613, 0x00C0)?;
            /* Unmute the AIF1 Timeslot 1 DAC2 path */
            conn.write(0x422, 0x0010)?;
        }

        Ok(())
    }

    pub fn init(&mut self, i2c_3: &mut i2c::I2C<I2C3>, device: u16) -> I2cError {
        self.output_device = device & 0x00ff;
        self.output_enabled = self.output_device > 0;
        self.input_device = device & 0xff00;
        self.input_enabled = self.input_device > 0;
        i2c_3.connect::<u16, _>(self.ic2_address, |mut conn| {
            WM8994::reset_device(&mut conn)?;
            conn.write(0x102, 0x0003)?;
            conn.write(0x817, 0x0000)?;
            conn.write(0x102, 0x0000)?;
            /* Enable VMID soft start (fast), Start-up Bias Current Enabled */
            conn.write(0x39, 0x006C)?;
            /* Enable bias generator, Enable VMID */
            if self.input_enabled {
                conn.write(0x01, 0x0013)?;
            } else {
                conn.write(0x01, 0x0003)?;
            }
            system_clock::wait_ms(50);
            match OutputInputDevice::from_value(self.output_device) {
                OutputInputDevice::OutputDeviceSpeaker => WM8994::path_configuration_output_speaker(&mut conn),
                OutputInputDevice::OutputDeviceHeadphone => WM8994::path_configuration_output_headphone(&mut conn),
                OutputInputDevice::OutputDeviceBoth => WM8994::path_configuration_output_both(&mut conn, self.input_device == OutputInputDevice::InputDeviceDigitalMic1Mic2 as u16),
                _ => WM8994::path_configuration_output_auto(&mut conn),
            }?;
            match OutputInputDevice::from_value(self.input_device) {
                OutputInputDevice::InputDeviceDigitalMicrophone1 => WM8994::path_configuration_input_mic1(&mut conn),
                OutputInputDevice::InputDeviceDigitalMicrophone2 => WM8994::path_configuration_input_mic2(&mut conn),
                OutputInputDevice::InputDeviceDigitalMic1Mic2 => WM8994::path_configuration_input_mic_both(&mut conn),
                OutputInputDevice::InputDeviceInputLine1 => WM8994::path_configuration_input_line1(&mut conn),
                _ => Ok(()),
            }?;

            self.clock_configuration(&mut conn)?;
            self.cold_startup(&mut conn)?;
            /* Volume Control */
            self.set_volume(&mut conn, 100)?;

            Ok(())
        })
    }
}
