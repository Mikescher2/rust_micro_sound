use alloc_cortex_m::CortexMHeap;
use stm32f7::stm32f7x6::*;
use stm32f7_discovery::gpio::*;
use stm32f7_discovery::i2c::*;
use stm32f7_discovery::system_clock::*;
use stm32f7_discovery::*;

use crate::audio::*;
use crate::graphics::*;
use crate::wm8994::*;

pub static CLOCKSPEED: f32 = 32_000.0;

pub static mut GLOB_SAI2: Option<SAI2> = None;

pub struct Hardware {
    rng: RNG,

    rand_state: (u32, u32), // <last_number, counter>

    touch: I2C<I2C3>,

    pub graphics: Graphics,
    pub audio: AudioMan,
    pub wm8994: WM8994,
    pub nvic: NVIC,
    pub nvic_stir: NVIC_STIR,
    pub tim6: TIM6,
}

impl Hardware {
    fn init_tim6(rcc: &RCC, tim: &TIM6) {
        // enable timers
        rcc.apb1enr.modify(|_, w| w.tim6en().enabled());
        // configure timer
        // clear update event
        tim.sr.modify(|_, w| w.uif().clear_bit());

        // setup timing
        tim.psc.modify(|_, w| unsafe { w.psc().bits(42000) });
        tim.arr.modify(|_, w| unsafe { w.arr().bits(3000) });

        // enable interrupt
        tim.dier.modify(|_, w| w.uie().set_bit());
        // start the timer counter
        tim.cr1.modify(|_, w| w.cen().set_bit());
    }

    pub fn new(alloc: &CortexMHeap, heap_size: usize, double_buffered: bool) -> Self {
        let core_peripherals = CorePeripherals::take().unwrap();
        let peripherals = Peripherals::take().unwrap();

        let mut p_syst = core_peripherals.SYST;
        let p_nvic = core_peripherals.NVIC;

        let mut p_rcc = peripherals.RCC;
        let mut p_pwr = peripherals.PWR;
        let mut p_flash = peripherals.FLASH;
        let mut p_ltdc = peripherals.LTDC;
        let mut p_fmc = peripherals.FMC;
        let mut p_sai_2 = peripherals.SAI2;
        let mut p_rng = peripherals.RNG;
        let p_nvic_stir = peripherals.NVIC_STIR;

        init::init_system_clock_216mhz(&mut p_rcc, &mut p_pwr, &mut p_flash);
        init::enable_gpio_ports(&mut p_rcc);

        let gpio_a = GpioPort::new(peripherals.GPIOA);
        let gpio_b = GpioPort::new(peripherals.GPIOB);
        let gpio_c = GpioPort::new(peripherals.GPIOC);
        let gpio_d = GpioPort::new(peripherals.GPIOD);
        let gpio_e = GpioPort::new(peripherals.GPIOE);
        let gpio_f = GpioPort::new(peripherals.GPIOF);
        let gpio_g = GpioPort::new(peripherals.GPIOG);
        let gpio_h = GpioPort::new(peripherals.GPIOH);
        let gpio_i = GpioPort::new(peripherals.GPIOI);
        let gpio_j = GpioPort::new(peripherals.GPIOJ);
        let gpio_k = GpioPort::new(peripherals.GPIOK);
        let mut pins = init::pins(gpio_a, gpio_b, gpio_c, gpio_d, gpio_e, gpio_f, gpio_g, gpio_h, gpio_i, gpio_j, gpio_k);

        init::init_systick(Hz(CLOCKSPEED as usize), &mut p_syst, &p_rcc);
        p_syst.enable_interrupt();

        init::init_sdram(&mut p_rcc, &mut p_fmc);
        let mut lcd = init::init_lcd(&mut p_ltdc, &mut p_rcc);
        pins.display_enable.set(true);
        pins.backlight.set(true);

        let layer_1 = lcd.layer_1().unwrap(); // background - draw
        let mut layer_2 = lcd.layer_2().unwrap(); // foreground - text

        //layer_1.clear();
        layer_2.clear();
        lcd::init_stdout_with_color(layer_2, 0x80);

        // Initialize the allocator BEFORE you use it
        unsafe {
            alloc.init(cortex_m_rt::heap_start() as usize, heap_size);
        }

        let mut i2c_3 = init::init_i2c_3(peripherals.I2C3, &mut p_rcc);
        i2c_3.test_1();
        i2c_3.test_2();

        //audio
        init::init_sai_2_out(&mut p_sai_2, &mut p_rcc);
        let mut wm8994 = WM8994::new(Frequency::AudioFrequency16K);
        wm8994.init(&mut i2c_3, OutputInputDevice::OutputDeviceHeadphone as u16).expect("WM8994 init failed");

        //touch::check_family_id(&mut i2c_3).unwrap();

        let rand = static_init_random(&mut p_rng, &mut p_rcc).expect("Random init failed");
        let tim = peripherals.TIM6;
        Hardware::init_tim6(&p_rcc, &tim);

        unsafe {
            GLOB_SAI2 = Some(p_sai_2);
        }

        Hardware {
            rng: p_rng,
            rand_state: rand,
            wm8994,
            tim6: tim,
            nvic: p_nvic,
            nvic_stir: p_nvic_stir,

            touch: i2c_3,

            graphics: Graphics::new(layer_1, double_buffered),
            audio: AudioMan::new(),
        }

        // The interrupt module needs an allocator for its dynamic interrupt table.
    }

    pub fn rand(&mut self) -> Result<u32, random::ErrorType> {
        let status = self.rng.sr.read();

        if status.ceis().bit_is_set() {
            self.reset_rand();
            return Err(random::ErrorType::ClockErrorInterrupt);
        }
        if status.seis().bit_is_set() {
            self.reset_rand();
            return Err(random::ErrorType::SeedErrorInterrupt);
        }

        if status.cecs().bit_is_set() {
            return Err(random::ErrorType::ClockError);
        }
        if status.secs().bit_is_set() {
            self.reset_rand();
            return Err(random::ErrorType::SeedError);
        }
        if status.drdy().bit_is_set() {
            let data = self.rng.dr.read().rndata().bits();
            if data != self.rand_state.0 {
                self.rand_state = (data, 0);
                return Ok(data);
            }
        }
        self.rand_state.1 += 1;
        if self.rand_state.1 > 80 {
            self.reset_rand();
            self.rand_state.1 = 0;
        }
        // data was not ready, try again!
        Err(random::ErrorType::NotReady)
    }

    pub fn reset_rand(&mut self) {
        self.rng.cr.modify(|_, w| w.rngen().clear_bit());
        self.rng.cr.modify(|_, w| w.ie().clear_bit());
        self.rng.cr.modify(|_, w| w.rngen().set_bit());
    }

    pub fn get_touches(&mut self) -> [Option<(u16, u16)>; 5] {
        let mut r = [None; 5];
        let mut i = 0;
        for t in &touch::touches(&mut self.touch).unwrap() {
            r[i] = Some((t.x, t.y));
            i += 1;
            if i == 5 {
                break;
            }
        }
        r
    }
}

pub fn static_init_random(rng: &mut RNG, rcc: &mut RCC) -> Option<(u32, u32)> {
    let control_register = rng.cr.read().rngen();
    if control_register.bit_is_set() {
        return None;
    }

    rcc.ahb2enr.modify(|_, w| w.rngen().set_bit());

    rng.cr.modify(|_, w| {
        w.ie().clear_bit();
        w.rngen().set_bit();
        w
    });

    Some((0x0, 0x0))
}
