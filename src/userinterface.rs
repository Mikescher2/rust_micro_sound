use stm32f7_discovery::*;

use crate::audio::*;
use crate::consts::*;
use crate::graphics::Graphics;
use crate::osc::BPM_CURRENT;
use crate::util::Util;

const PI: f32 = 3.141_596;
const TAU: f32 = 2.0 * PI;

pub struct UserInterface {
    pub grid_play_point: Option<(u8, u8)>,
    pub grid_play_point_changed: bool,
    pub grid_play_point_size: usize,

    grid_graph_offset: i32,
    grid_points_active: [[u8; 16]; 16],

    beat_lasttime: usize,
    beat_delta: usize,
    beat_counter: i32,

    last_update_time: usize,

    pub button_rec: Button,
    pub button_del: Button,
    pub button_metro: Button,
    pub button_note: Button,
    pub button_note_up: Button,
    pub button_note_down: Button,

    pub button_dec: Button,
    pub button_inc: Button,

    pub button_square: Button,
    pub button_sin: Button,
    pub button_sawtooth: Button,
    pub button_sin3: Button,
    pub button_noise: Button,
    pub button_peak: Button,

    pub generator: AudioGenerator,
    pub generator_changed: bool,

    pointer: [Option<(f32, f32)>; 5],

    pub amp_index: usize,
    pub amp_redraw: bool,
    pub amp_changed: bool,

    fps_counter: u32,
    fps_lasttime: usize,
    fps_value: f32,
    fps_needs_refresh: bool,

    multimode: bool,
}

impl UserInterface {
    pub fn new() -> Self {
        let beat_delta: usize;
        unsafe {
            beat_delta = 1000 / (BPM_CURRENT / 60) as usize;
        }
        UserInterface {
            grid_graph_offset: 0,
            grid_play_point: None,
            grid_play_point_changed: false,
            grid_play_point_size: 2,
            grid_points_active: [[0; 16]; 16],

            amp_index: 80,
            amp_redraw: true,
            amp_changed: true,

            generator: AudioGenerator::new1(SingleAudioGenerator::Sin),
            generator_changed: false,

            beat_lasttime: 0,
            beat_delta: beat_delta,
            beat_counter: 0,

            last_update_time: 0,

            button_dec: Button::new(8, 480 - 8 - 14, 14, 14),
            button_inc: Button::new(8 + 242, 480 - 8 - 14, 14, 14),

            button_rec: Button::new(8, 480 - 8 - 22 - 8 - 48, 48, 48),
            button_del: Button::new(8 + 48 + 8, 480 - 8 - 22 - 8 - 48, 48, 48),
            button_metro: Button::new(270 - 8 - 48, 480 - 8 - 22 - 8 - 48, 48, 48),
            button_note: Button::new(270 - 8 - 48 - 8 - 48, 480 - 8 - 22 - 8 - 48, 48, 48),
            button_note_up: Button::new(270 - 8 - 48 - 8 - 48, 480 - 8 - 22 - 8 - 24, 48, 24),
            button_note_down: Button::new(270 - 8 - 48 - 8 - 48, 480 - 8 - 22 - 8 - 48, 48, 24),

            button_square: Button::new(8, 8 + 16 * 16 + 8 + 48 + 8, 48, 48),
            button_sin: Button::new(8 + 48 + 8, 8 + 16 * 16 + 8 + 48 + 8, 48, 48),
            button_sawtooth: Button::new(8 + 48 + 8 + 48 + 8, 8 + 16 * 16 + 8 + 48 + 8, 48, 48),
            button_sin3: Button::new(8, 8 + 16 * 16 + 8, 48, 48),
            button_noise: Button::new(8 + 48 + 8, 8 + 16 * 16 + 8, 48, 48),
            button_peak: Button::new(8 + 48 + 8 + 48 + 8, 8 + 16 * 16 + 8, 48, 48),

            pointer: [None; 5],

            fps_counter: 0,
            fps_lasttime: 0,
            fps_value: 999.0,
            fps_needs_refresh: true,

            multimode: false,
        }
    }

    pub fn update(&mut self, total_time: usize, touches: [Option<(f32, f32)>; 5]) {
        let delta = total_time - self.last_update_time;

        self.grid_graph_offset = (total_time / 20) as i32;

        if self.beat_lasttime + delta >= self.beat_delta {
            self.beat_lasttime = delta;
            self.beat_counter = (self.beat_counter + 1) % 16;
        } else {
            self.beat_lasttime += delta;
        }

        self.fps_counter += 1;
        if self.fps_counter >= 16 && (total_time - self.fps_lasttime) > 1000 {
            self.fps_value = (self.fps_counter as f32 * 1_000.0) / ((total_time - self.fps_lasttime) as f32);
            self.fps_counter = 0;
            self.fps_needs_refresh = true;
            self.fps_lasttime = total_time;
        }

        self.button_inc.update(touches);
        self.button_dec.update(touches);
        self.button_rec.update(touches);
        self.button_sin.update(touches);
        self.button_square.update(touches);
        self.button_sawtooth.update(touches);
        self.button_sin3.update(touches);
        self.button_noise.update(touches);
        self.button_peak.update(touches);
        self.button_del.update(touches);
        self.button_note.update(touches);
        self.button_note_up.update(touches);
        self.button_note_down.update(touches);
        self.button_metro.update(touches);

        if self.button_inc.is_just_down() {
            unsafe {
                BPM_CURRENT = Util::min((BPM_CURRENT + 2) as i32, BPM_MAX as i32) as usize;
                self.update_bpm();
            }
        }
        if self.button_dec.is_just_down() {
            unsafe {
                BPM_CURRENT = Util::max((BPM_CURRENT - 2) as i32, BPM_MIN as i32) as usize;
                self.update_bpm();
            }
        }

        if self.multimode {
            if self.button_sin.is_just_down() {
                self.generator = self.generator.toggle(SingleAudioGenerator::Sin);
                self.generator_changed = true;
            } else if self.button_square.is_just_down() {
                self.generator = self.generator.toggle(SingleAudioGenerator::Square);
                self.generator_changed = true;
            } else if self.button_sawtooth.is_just_down() {
                self.generator = self.generator.toggle(SingleAudioGenerator::Saw);
                self.generator_changed = true;
            } else if self.button_sin3.is_just_down() {
                self.generator = self.generator.toggle(SingleAudioGenerator::Sin3);
                self.generator_changed = true;
            } else if self.button_noise.is_just_down() {
                self.generator = self.generator.toggle(SingleAudioGenerator::Noise);
                self.generator_changed = true;
            } else if self.button_peak.is_just_down() {
                self.generator = self.generator.toggle(SingleAudioGenerator::Triangle);
                self.generator_changed = true;
            } else if self.button_del.is_just_down() {
                self.generator = self.generator.toggle(SingleAudioGenerator::Zero);
                self.generator_changed = true;
            } else {
                self.generator_changed = false;
            }
        } else {
            if self.button_sin.is_just_down() {
                self.generator = AudioGenerator::new1(SingleAudioGenerator::Sin);
                self.generator_changed = true;
            } else if self.button_square.is_just_down() {
                self.generator = AudioGenerator::new1(SingleAudioGenerator::Square);
                self.generator_changed = true;
            } else if self.button_sawtooth.is_just_down() {
                self.generator = AudioGenerator::new1(SingleAudioGenerator::Saw);
                self.generator_changed = true;
            } else if self.button_sin3.is_just_down() {
                self.generator = AudioGenerator::new1(SingleAudioGenerator::Sin3);
                self.generator_changed = true;
            } else if self.button_noise.is_just_down() {
                self.generator = AudioGenerator::new1(SingleAudioGenerator::Noise);
                self.generator_changed = true;
            } else if self.button_peak.is_just_down() {
                self.generator = AudioGenerator::new1(SingleAudioGenerator::Triangle);
                self.generator_changed = true;
            } else if self.button_del.is_just_down() {
                self.generator = AudioGenerator::new1(SingleAudioGenerator::Zero);
                self.generator_changed = true;
            } else {
                self.generator_changed = false;
            }
        }

        if self.button_sin.is_double_down()
            || self.button_square.is_double_down()
            || self.button_sawtooth.is_double_down()
            || self.button_sin3.is_double_down()
            || self.button_noise.is_double_down()
            || self.button_peak.is_double_down()
        {
            self.multimode = !self.multimode;
            if !self.multimode {
                self.generator = AudioGenerator::new1(self.generator.single());
                self.generator_changed = true;
            }
        }

        if self.generator_changed {
            self.button_sin.invalidate();
            self.button_square.invalidate();
            self.button_sawtooth.invalidate();
            self.button_sin3.invalidate();
            self.button_noise.invalidate();
            self.button_peak.invalidate();
        }

        let gpa_step = Util::max(1, (delta as i32) / 2);
        for x in 0..16 {
            for y in 0..16 {
                if self.grid_points_active[y][x] > 0 {
                    self.grid_points_active[y][x] = Util::max(0, i32::from(self.grid_points_active[y][x]) - gpa_step) as u8;
                }
            }
        }

        let mut gpp = None;
        for opttouch in touches.iter() {
            if let Some(touch) = opttouch {
                let gx = (touch.0 - 8.0) / 16.0;
                let gy = (touch.1 - 8.0) / 16.0;
                if gx >= 0.0 && gy >= 0.0 && gx < 16.0 && gy < 16.0 {
                let gpps = self.grid_play_point_size as u32;
                let (gx2,gy2) = (gpps*(gx as u32 /gpps), gpps*(gy as u32 /gpps));
                    self.grid_points_active[gy2 as usize][gx2 as usize] = 255;
                    gpp = Some((gy as u8, gx as u8));
                }
            }
        }
        if self.grid_play_point != gpp {
            self.grid_play_point = gpp;
            self.grid_play_point_changed = true;
        } else {
            self.grid_play_point_changed = false;
        }

        for opttouch in touches.iter() {
            if let Some(touch) = opttouch {
                if touch.0 > 272.0 - 8.0 - 8.0 - 8.0 - 48.0 && touch.0 < 272.0 - 8.0 - 8.0 - 8.0 {
                    let vy = touch.1 - (8.0 + 256.0 + 8.0);
                    if vy <= 112.0 && vy >= -8.0 {
                        let viy = Util::max_u32(0, Util::min_u32(97, vy as u32));

                        self.amp_index = viy as usize;
                        self.amp_redraw = true;
                        self.amp_changed = true;
                        AudioMan::set_amp(VOL_CURVE[self.amp_index]);
                    }
                }
            }
        }

        self.pointer = touches;
        self.last_update_time = total_time;
    }

    pub fn update_bpm(&mut self) {
        unsafe {
            self.beat_delta = 1000 / (BPM_CURRENT / 60);
        }
    }

    pub fn draw_graph(&self, graphics: &mut Graphics, freq: f32) {
        graphics.transformation_push();
        {
            graphics.transform_offset(8.0, 8.0);
            graphics.enable_grid(8, 8, 16*self.grid_play_point_size as i32, 0x40_40_40);
            for x in 0..16 {
                for y in 0..16 {
                    let x2 = self.grid_play_point_size * (x/self.grid_play_point_size);
                    let y2 = self.grid_play_point_size * (y/self.grid_play_point_size);
                    let c = if self.grid_points_active[y2][x2] == 0 {
                        let d = Util::dist8(x2 as i32, y2 as i32, 0);
                        let p = d / 10.0;
                        let frc = (0x2E * ((p * 256.0) as u32)) / 255;
                        (frc << 16) | (frc << 8) | frc
                    } else {
                        (((0xFF * u32::from(self.grid_points_active[y2][x2])) / 255) << 16)
                            | (((0x00 * self.grid_points_active[y2][x2] as u32) / 255) << 8)
                            | ((0x00 * self.grid_points_active[y2][x2] as u32) / 255)
                    };

                    graphics.fill_rect(x as f32 * 16.0 + 1.0, y as f32 * 16.0 + 1.0, if x == 15 { 15.0 } else { 16.0 }, if y == 15 { 15.0 } else { 16.0 }, c);
                }
            }
            graphics.disable_grid();

            graphics.transformation_push();
            {
                graphics.transform_offset(0.0, 128.0);

                graphics.transform_scale(256.0 / (2.0 * TAU), 64.0);

                let sin_off = (self.grid_graph_offset as f32) / 12.0;
                let sin_mul = Util::sin((self.grid_graph_offset as f32) / 12.0);

                //if self.generator == AudioGenerator::Noise {
                //    graphics.fill_function_mirror(0.0, 2.0 * TAU, |x| Util::noise(x), COL_GRAPH)
                //} else if self.generator == AudioGenerator::Zero {
                //    // ABSOLUTELY NOTHING
                //} else {
                //    let fun = AudioGenerator::to_function(&self.generator);
                //    graphics.fill_function(0.0, 2.0 * TAU, |x| (fun(((x + sin_off) * 20.0) as u32, freq) - 1.0) * sin_mul, COL_GRAPH);
                //}

                let gen = self.generator;

                graphics.fill_function(0.0, 2.0 * TAU, |x| (gen.calc(((x + sin_off) * 20.0) as u32, freq) - 1.0) * sin_mul, COL_GRAPH);

                //match self.generator {
                //    AudioGenerator::Sin => graphics.fill_function(0.0, 2.0 * TAU, |x| Util::sin(x + sin_off) * sin_mul, COL_GRAPH),
                //    AudioGenerator::Sin3 => graphics.fill_function(0.0, 2.0 * TAU, |x| Util::sin3(x + sin_off) * sin_mul, COL_GRAPH),
                //    AudioGenerator::Saw => graphics.fill_function(0.0, 2.0 * TAU, |x| Util::saw(x + sin_off) * sin_mul, COL_GRAPH),
                //    AudioGenerator::Square => graphics.fill_function(0.0, 2.0 * TAU, |x| Util::square(x + sin_off) * sin_mul, COL_GRAPH),
                //    AudioGenerator::Zero => graphics.fill_function(0.0, 2.0 * TAU, |x| Util::zero(x + sin_off) * sin_mul, COL_GRAPH),
                //    AudioGenerator::Noise => graphics.fill_function_mirror(0.0, 2.0 * TAU, |x| Util::noise(x + sin_off), COL_GRAPH),
                //    AudioGenerator::Triangle => graphics.fill_function(0.0, 2.0 * TAU, |x| Util::triangle(x + sin_off) * sin_mul, COL_GRAPH),
                //}

                //graphics.draw_function(0.0, 2.0 * TAU, |x| math::sinf(x + sin_off) * sin_mul, COL_GRAPH);

                //graphics.fill_function(0.0, 2.0 * TAU, |x| (((x + sin_off) as i32)%2) as f32, COL_GRAPH);
                //graphics.draw_function(0.0, 2.0 * TAU, |x| (((x + sin_off) as i32)%2) as f32, COL_GRAPH);
            }
            graphics.transformation_pop();

            graphics.draw_rect(0.0, 0.0, 256.0, 256.0, COL_GRAPH)
        }
        graphics.transformation_pop();
    }

    pub fn draw_beat(&self, g: &mut Graphics) {
        g.transformation_push();
        {
            self.draw_button_obj(g, &self.button_inc, COL_BEAT_BUTTON_BORDER, COL_BEAT_BUTTON_FILL, COL_BEAT_BUTTON_PRESSED);
            self.draw_button_obj(g, &self.button_dec, COL_BEAT_BUTTON_BORDER, COL_BEAT_BUTTON_FILL, COL_BEAT_BUTTON_PRESSED);

            g.transform_offset(8.0, (480 - 8 - 14) as f32);

            for i in 0..16 {
                self.draw_button(
                    g,
                    (14 + 2 + i * 14, 0),
                    (14, 14),
                    COL_BEAT_BORDER,
                    if i == self.beat_counter % 16 {
                        if i % 4 == 0 {
                            COL_BEAT_FILL_ON_ALT
                        } else {
                            COL_BEAT_FILL_ON
                        }
                    } else {
                        COL_BEAT_FILL_OFF
                    },
                );
            }
        }
        g.transformation_pop();
    }

    pub fn draw_fps(&mut self) {
        if self.fps_needs_refresh {
            self.fps_needs_refresh = false;

            stm32f7_discovery::lcd::stdout::clear_stdout();
            println!("> {:.2} FPS", self.fps_value);
        }
    }

    pub fn draw_recorder(&mut self, g: &mut Graphics) {
        if self.button_rec.pop_redraw() {
            self.draw_imgbutton_obj_2(g, &self.button_rec, &IMG_REC, COL_REC_BUTTON_BORDER, COL_REC_FOREGROUND, COL_REC_BUTTON_FILL, COL_REC_BUTTON_PRESSED);
        }
        if self.button_del.pop_redraw() {
            self.draw_imgbutton_obj_2(g, &self.button_del, &IMG_DEL, COL_REC_BUTTON_BORDER, COL_REC_FOREGROUND, COL_REC_BUTTON_FILL, COL_REC_BUTTON_PRESSED);
        }
        if self.button_metro.pop_redraw() {
            self.draw_imgbutton_obj_2(
                g,
                &self.button_metro,
                &IMG_METRONOME,
                COL_REC_BUTTON_BORDER,
                COL_REC_FOREGROUND,
                COL_REC_BUTTON_FILL,
                COL_REC_BUTTON_PRESSED,
            );
        }
        if self.button_note_up.pop_redraw() || self.button_note_down.pop_redraw() {
            self.draw_imgbutton_obj_3(
                g,
                &self.button_note,
                &IMG_UPDOWN,
                COL_REC_BUTTON_BORDER,
                COL_REC_FOREGROUND,
                COL_REC_BUTTON_FILL,
                COL_REC_BUTTON_PRESSED,
                self.button_note_up.is_down() || self.button_note_down.is_down(),
            );
        }
    }

    pub fn draw_touches(&self, g: &mut Graphics, len: f32) {
        for i in 0..5 {
            if let Some(touch) = self.pointer[i] {
                g.draw_line(touch.0 - len / 2.0, touch.1, touch.0 + len / 2.0, touch.1, COL_TOUCH_CROSSHAIR);
                g.draw_line(touch.0, touch.1 - len / 2.0, touch.0, touch.1 + len / 2.0, COL_TOUCH_CROSSHAIR);
            }
        }
    }

    pub fn draw_audiobuffer(&self, g: &mut Graphics, playback: u32, generate: u32, semi_refresh: bool) {
        let mut col = 0x33_FF_33;
        if semi_refresh {
            col = 0x33_33_FF;
        }

        g.fill_rect(272.0 - 8.0 - 8.0, 8.0 + 256.0 + 8.0, 8.0, 104.0, 0x33_33_33);
        g.fill_rect(
            272.0 - 8.0 - 8.0 + 1.0,
            8.0 + 256.0 + 8.0 + 1.0,
            8.0 - 2.0,
            (102.0 * (generate as f32 - playback as f32)) / (16_384.0),
            col,
        );
    }

    pub fn draw_volume(&mut self, g: &mut Graphics) {
        if !self.amp_redraw {
            return;
        }

        g.fill_image_rect2(272.0 - 8.0 - 8.0 - 8.0 - 48.0, 8.0 + 256.0 + 8.0, 48.0, 104.0, &IMG_EAR, 0x33_33_33, 0x11_11_11, 10);

        g.fill_rect(272.0 - 8.0 - 8.0 - 8.0 - 48.0, 8.0 + 256.0 + 8.0 + (self.amp_index as f32), 48.0, 7.0, 0xFF_00_00);

        self.amp_redraw = false;
    }

    pub fn draw_generator_buttons(&mut self, g: &mut Graphics) {
        if self.button_sin.pop_redraw() {
            self.draw_imgbutton_obj(
                g,
                &self.button_sin,
                &IMG_SIN,
                if self.multimode { COL_AGEN_BUTTON_MULTIBORDER } else { COL_AGEN_BUTTON_BORDER },
                if self.multimode { COL_AGEN_BUTTON_MULTIFOREGROUND } else { COL_AGEN_BUTTON_FOREGROUND },
                if self.generator.contains(SingleAudioGenerator::Sin) {
                    COL_AGEN_BUTTON_FILL_ON
                } else {
                    COL_AGEN_BUTTON_FILL_OFF
                },
                COL_AGEN_BUTTON_PRESSED,
            );
        }
        if self.button_square.pop_redraw() {
            self.draw_imgbutton_obj(
                g,
                &self.button_square,
                &IMG_SQUARE,
                if self.multimode { COL_AGEN_BUTTON_MULTIBORDER } else { COL_AGEN_BUTTON_BORDER },
                if self.multimode { COL_AGEN_BUTTON_MULTIFOREGROUND } else { COL_AGEN_BUTTON_FOREGROUND },
                if self.generator.contains(SingleAudioGenerator::Square) {
                    COL_AGEN_BUTTON_FILL_ON
                } else {
                    COL_AGEN_BUTTON_FILL_OFF
                },
                COL_AGEN_BUTTON_PRESSED,
            );
        }
        if self.button_sawtooth.pop_redraw() {
            self.draw_imgbutton_obj(
                g,
                &self.button_sawtooth,
                &IMG_SAW,
                if self.multimode { COL_AGEN_BUTTON_MULTIBORDER } else { COL_AGEN_BUTTON_BORDER },
                if self.multimode { COL_AGEN_BUTTON_MULTIFOREGROUND } else { COL_AGEN_BUTTON_FOREGROUND },
                if self.generator.contains(SingleAudioGenerator::Saw) {
                    COL_AGEN_BUTTON_FILL_ON
                } else {
                    COL_AGEN_BUTTON_FILL_OFF
                },
                COL_AGEN_BUTTON_PRESSED,
            );
        }
        if self.button_sin3.pop_redraw() {
            self.draw_imgbutton_obj(
                g,
                &self.button_sin3,
                &IMG_SIN3,
                if self.multimode { COL_AGEN_BUTTON_MULTIBORDER } else { COL_AGEN_BUTTON_BORDER },
                if self.multimode { COL_AGEN_BUTTON_MULTIFOREGROUND } else { COL_AGEN_BUTTON_FOREGROUND },
                if self.generator.contains(SingleAudioGenerator::Sin3) {
                    COL_AGEN_BUTTON_FILL_ON
                } else {
                    COL_AGEN_BUTTON_FILL_OFF
                },
                COL_AGEN_BUTTON_PRESSED,
            );
        }
        if self.button_noise.pop_redraw() {
            self.draw_imgbutton_obj(
                g,
                &self.button_noise,
                &IMG_RAND,
                if self.multimode { COL_AGEN_BUTTON_MULTIBORDER } else { COL_AGEN_BUTTON_BORDER },
                if self.multimode { COL_AGEN_BUTTON_MULTIFOREGROUND } else { COL_AGEN_BUTTON_FOREGROUND },
                if self.generator.contains(SingleAudioGenerator::Noise) {
                    COL_AGEN_BUTTON_FILL_ON
                } else {
                    COL_AGEN_BUTTON_FILL_OFF
                },
                COL_AGEN_BUTTON_PRESSED,
            );
        }
        if self.button_peak.pop_redraw() {
            self.draw_imgbutton_obj(
                g,
                &self.button_peak,
                &IMG_PEAK,
                if self.multimode { COL_AGEN_BUTTON_MULTIBORDER } else { COL_AGEN_BUTTON_BORDER },
                if self.multimode { COL_AGEN_BUTTON_MULTIFOREGROUND } else { COL_AGEN_BUTTON_FOREGROUND },
                if self.generator.contains(SingleAudioGenerator::Triangle) {
                    COL_AGEN_BUTTON_FILL_ON
                } else {
                    COL_AGEN_BUTTON_FILL_OFF
                },
                COL_AGEN_BUTTON_PRESSED,
            );
        }
    }

    pub fn draw_imgbutton(&self, g: &mut Graphics, mask: &[u64; 48], pos: (i32, i32), size: (i32, i32), col_border: u32, col_back: u32, col_fill: u32) {
        g.fill_image_rect(pos.0 as f32 + 1.0, pos.1 as f32 + 1.0, size.0 as f32 - 1.0, size.1 as f32 - 1.0, mask, col_back, col_fill);
        g.draw_rect(pos.0 as f32 + 0.0, pos.1 as f32 + 0.0, size.0 as f32, size.1 as f32, col_border);
    }

    pub fn draw_button(&self, g: &mut Graphics, pos: (i32, i32), size: (i32, i32), col_border: u32, col_fill: u32) {
        g.fill_rect(pos.0 as f32 + 1.0, pos.1 as f32 + 1.0, size.0 as f32 - 1.0, size.1 as f32 - 1.0, col_fill);
        g.draw_rect(pos.0 as f32 + 0.0, pos.1 as f32 + 0.0, size.0 as f32, size.1 as f32, col_border);
    }

    pub fn draw_button_obj(&self, g: &mut Graphics, btn: &Button, col_border: u32, col_fill: u32, col_press: u32) {
        self.draw_button(
            g,
            (i32::from(btn.x), i32::from(btn.y)),
            (i32::from(btn.w), i32::from(btn.h)),
            col_border,
            if btn.is_down() { col_press } else { col_fill },
        );
    }

    pub fn draw_imgbutton_obj(&self, g: &mut Graphics, btn: &Button, mask: &[u64; 48], col_border: u32, col_fill: u32, col_fore: u32, col_press: u32) {
        self.draw_imgbutton(
            g,
            mask,
            (i32::from(btn.x), i32::from(btn.y)),
            (i32::from(btn.w), i32::from(btn.h)),
            col_border,
            if btn.is_down() { col_press } else { col_fill },
            col_fore,
        );
    }

    pub fn draw_imgbutton_obj_2(&self, g: &mut Graphics, btn: &Button, mask: &[u64; 48], col_border: u32, col_fill: u32, col_fore: u32, col_press: u32) {
        self.draw_imgbutton(
            g,
            mask,
            (i32::from(btn.x), i32::from(btn.y)),
            (i32::from(btn.w), i32::from(btn.h)),
            col_border,
            col_fill,
            if btn.is_down() { col_press } else { col_fore },
        );
    }

    pub fn draw_imgbutton_obj_3(&self, g: &mut Graphics, btn: &Button, mask: &[u64; 48], col_border: u32, col_fill: u32, col_fore: u32, col_press: u32, down: bool) {
        self.draw_imgbutton(
            g,
            mask,
            (i32::from(btn.x), i32::from(btn.y)),
            (i32::from(btn.w), i32::from(btn.h)),
            col_border,
            col_fill,
            if down { col_press } else { col_fore },
        );
    }

    pub fn explode(&mut self) {
        for x in 0..16 {
            for y in 0..16 {
                self.grid_points_active[y][x] = 255;
            }
        }
    }
}

#[derive(Default)]
pub struct Button {
    x: u16,
    y: u16,
    w: u16,
    h: u16,

    down: bool,
    down_prev: bool,
    down_double: bool,

    last_down: usize,

    redraw: bool,
}

impl Button {
    pub fn new(x: u16, y: u16, w: u16, h: u16) -> Self {
        Button {
            down: false,
            down_prev: false,
            down_double: false,
            last_down: 0,

            x,
            y,
            w,
            h,

            redraw: true,
        }
    }

    pub fn update(&mut self, touches: [Option<(f32, f32)>; 5]) {
        self.down_prev = self.down;
        self.down = false;
        self.down_double = false;
        for opttouch in touches.iter() {
            if let Some(touch) = opttouch {
                self.down = Util::in_rect(self.x, self.y, self.w, self.h, *touch);
                if self.down {
                    break;
                }
            }
        }
        if self.down_prev != self.down {
            self.redraw = true;
        }
        if self.is_just_down() {
            let time = system_clock::ms();
            self.down_double = time - self.last_down < 250;
            self.last_down = time;
        }
    }

    pub fn is_down(&self) -> bool {
        self.down
    }

    /* false if the button press is a repeated button press */
    pub fn is_just_down(&self) -> bool {
        self.down && !self.down_prev
    }

    pub fn is_double_down(&self) -> bool {
        self.down_double
    }

    pub fn invalidate(&mut self) {
        self.redraw = true;
    }

    pub fn pop_redraw(&mut self) -> bool {
        if self.redraw {
            self.redraw = false;
            return true;
        }
        false
    }
}
