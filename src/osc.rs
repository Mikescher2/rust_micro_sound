use alloc_cortex_m::CortexMHeap;
use stm32f7_discovery::*;

use crate::audio::*;
use crate::consts::*;
use crate::hardware::Hardware;
use crate::recorder::{Recorder, RecorderState};
use crate::userinterface::UserInterface;
use crate::util::*;

pub static mut RECORDER: Option<Recorder> = None;
pub static mut BPM_CURRENT: usize = 120;

pub struct Osc {
    hw: Hardware,
    ui: UserInterface,

    audio_freq: f32,
    audio_method: AudioGenerator,
    last_audio_freq: f32,

    note_scale_offset: usize,
    note_scale_grid_width: f32,

    lfo_freq: f32,
    lfo_method: SingleAudioGenerator,

    bpm_latest_timestamps: [usize; 4],
}

impl Osc {
    pub fn new(allocator: &CortexMHeap, heap_size: usize, double_buffered: bool) -> Self {
        let mut hw = Hardware::new(&allocator, heap_size, double_buffered);

        unsafe {
            RECORDER.replace(Recorder::new());
        }

        hw.graphics.clear(COL_BACKGROUND);
        hw.graphics.flush(true);
        // Set up coordinate system
        hw.graphics.transform_rotate_90_ccw();
        hw.graphics.transform_scale(-1.0, 1.0);

        let ui = UserInterface::new();

        AudioMan::set_amp(VOL_CURVE[ui.amp_index]);

        Osc {
            hw: hw,
            ui: ui,
            audio_freq: NOTES[60],
            last_audio_freq: NOTES[60],
            audio_method: AudioGenerator::new0(),
            lfo_freq: 10.,
            lfo_method: SingleAudioGenerator::Triangle,
            bpm_latest_timestamps: [0, 0, 0, 0],
            note_scale_offset: 60,
            note_scale_grid_width: 8.,
        }
    }

    pub fn update(&mut self) {
        self.ui.draw_graph(&mut self.hw.graphics, self.last_audio_freq);
        self.ui.draw_beat(&mut self.hw.graphics);
        self.ui.draw_recorder(&mut self.hw.graphics);
        self.ui.draw_generator_buttons(&mut self.hw.graphics);
        //self.ui.draw_touches(&mut self.hw.graphics, 48.0);
        self.ui.draw_audiobuffer(
            &mut self.hw.graphics,
            AudioMan::get_index_playback(),
            self.hw.audio.buffer_generate_position,
            self.hw.audio.buffer_semi_refresh,
        );
        self.ui.draw_volume(&mut self.hw.graphics);

        let touches_raw = self.hw.get_touches();
        let touches_conv = self.hw.graphics.convert_touches(touches_raw);
        self.ui.update(system_clock::ms(), touches_conv);

        if self.ui.grid_play_point_changed || self.ui.generator_changed {
            if let Some(pt) = self.ui.grid_play_point {
                self.audio_freq = self.get_frequency(pt.1 as u32);
                self.audio_method = self.ui.generator;
                //self.lfo_freq = if pt.0 >= 4 { ( 16. * (pt.0 as f32 - 4.) / 12.) as f32} else { 0. };
                unsafe{self.lfo_freq = if pt.0 >= 4 { ( (BPM_CURRENT as f32/ BPM_MIN as f32) * 16. * (pt.0 as f32 - 4.) / 12.) as f32} else { 0. };}
                self.last_audio_freq = self.audio_freq;
            } else {
                self.audio_method = AudioGenerator::new0();
                self.audio_freq = 0.0;
                self.lfo_freq = 0.0;
            }
            self.hw.audio.force_refresh();
        }

        if self.ui.amp_changed {
            self.hw.audio.force_refresh();
            self.ui.amp_changed = false;
        }

        unsafe {
            if let Some(rec) = &mut RECORDER {
                if self.ui.button_rec.is_down() {
                    rec.set_state(RecorderState::Record);
                } else if self.ui.button_del.is_down() {
                    rec.set_state(RecorderState::Clear);
                } else {
                    rec.set_state(RecorderState::None);
                }

                if self.ui.button_del.is_double_down() {
                    rec.clear_all();
                    self.ui.explode();
                }
            }
        }
        if self.ui.button_metro.is_just_down() {
            let current_time = system_clock::ms();
            for i in 0..3 {
                self.bpm_latest_timestamps[i] = self.bpm_latest_timestamps[i + 1];
            }
            self.bpm_latest_timestamps[3] = current_time;

            let mut dt_sum = 0;
            for i in 0..3 {
                dt_sum += self.bpm_latest_timestamps[i + 1] - self.bpm_latest_timestamps[i];
            }
            let dt = 3 * 60000 / dt_sum;
            unsafe {
                BPM_CURRENT = Util::max_u32(Util::min_u32(dt as u32, BPM_MAX as u32), BPM_MIN as u32) as usize;
            }
            self.ui.update_bpm();
        }

        if self.ui.button_note_up.is_just_down() {
            self.note_scale_offset = Util::min_u32(self.note_scale_offset as u32 + 12, NOTES.len() as u32 - 36) as usize;
        }
        if self.ui.button_note_down.is_just_down() {
            self.note_scale_offset = Util::max_u32(self.note_scale_offset as u32 - 12, 12) as usize;
        }
        if self.ui.button_note.is_double_down(){
            self.toggle_grid_size();
        }

        let lfo_freq = self.lfo_freq;
        let lfo_method = if lfo_freq == 0.0 { &AudioMan::one } else { SingleAudioGenerator::to_function(&self.lfo_method) };
        let audio_method = self.audio_method;
        self.hw
            .audio
            .fill_buffer(self.audio_freq, |amp, u, frequency| (lfo_method(u, lfo_freq) / 2.0) * (amp * audio_method.calc(u, frequency)));

        self.hw.graphics.flush(false);
    }

    fn toggle_grid_size(&mut self){
        self.note_scale_grid_width = if self.note_scale_grid_width == 16. {8.} else {16.};
        self.ui.grid_play_point_size = if self.ui.grid_play_point_size == 2 {1} else {2};
    }
    fn get_frequency(&self, touch_position: u32) -> f32{
        let position =(self.note_scale_grid_width * touch_position as f32 /16.) as usize;
        match position % 16 as usize {
            0 => NOTES[self.note_scale_offset + 0],
            1 => NOTES[self.note_scale_offset + 2],
            2 => NOTES[self.note_scale_offset + 4],
            3 => NOTES[self.note_scale_offset + 5],
            4 => NOTES[self.note_scale_offset + 7],
            5 => NOTES[self.note_scale_offset + 9],
            6 => NOTES[self.note_scale_offset + 11],
            7 => NOTES[self.note_scale_offset + 12],
            8 => NOTES[self.note_scale_offset + 14],
            9 => NOTES[self.note_scale_offset + 16],
            10 => NOTES[self.note_scale_offset + 17],
            11 => NOTES[self.note_scale_offset + 19],
            12 => NOTES[self.note_scale_offset + 21],
            13 => NOTES[self.note_scale_offset + 23],
            14 => NOTES[self.note_scale_offset + 24],
            _ => NOTES[self.note_scale_offset + 26],
        }
        //return NOTES[self.note_scale_offset + (self.note_scale_grid_width * touch_position as f32 /16.) as usize];
        //if self.note_scale_grid_width == 16. {
        //    return NOTES[self.note_scale_offset + (self.note_scale_grid_width * touch_position as f32 /16.) as usize];
        //}
    }

    pub fn run(&mut self) -> ! {
        //self.ui.button_rec.set_listener(self);
        loop {
            self.update()
        }
    }
}
