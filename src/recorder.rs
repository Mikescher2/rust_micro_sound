use crate::audio::BITRATE;
use crate::consts::*;
use crate::osc::BPM_CURRENT;
use crate::rambuffer::*;

pub enum RecorderState {
    Record,
    Clear,
    None,
}
pub struct Recorder {
    buffer: RamBuffer,
    num_writes: RamBuffer,
    state: RecorderState,
    size: usize,
    buffer_position: f32,
}

impl Recorder {
    pub fn new() -> Self {
        let size = (4 * 2 * BITRATE as usize * BARS_MAX) as usize;
        let offset = 0xC030_000c;
        let buffer = RamBuffer::new(offset, size);
        let num_writes = RamBuffer::new(offset + size as u32, size);
        Recorder::clear_buffer(&buffer, size);
        Recorder::set_buffer(&num_writes, size, 1);
        Recorder {
            buffer: buffer,
            num_writes: num_writes,
            state: RecorderState::None,
            size: size,
            buffer_position: 0.,
        }
    }

    pub fn update(&mut self, val: u32) -> u32 {
        match self.state {
            RecorderState::Record => self.record(val),
            RecorderState::Clear => self.clear(),
            RecorderState::None => (),
        }
        unsafe {
            self.buffer_position = (self.buffer_position + BPM_CURRENT as f32 / BPM_MIN as f32) % self.size as f32;
        }
        return val;
    }

    fn set_buffer(buffer: &RamBuffer, size: usize, value: u32) {
        for i in 0..size {
            buffer.set(i, value);
        }
    }

    fn clear_buffer(buffer: &RamBuffer, size: usize) {
        for i in 0..size {
            buffer.set(i, 0);
        }
    }

    pub fn clear_all(&self) {
        Recorder::set_buffer(&self.num_writes, self.size, 1);
        Recorder::clear_buffer(&self.buffer, self.size);
    }

    fn record(&self, val: u32) {
        unsafe {
            for position in self.buffer_position as usize..(self.buffer_position + BPM_CURRENT as f32 / BPM_MIN as f32) as usize {
                let buffer_pos = position % self.size;
                self.buffer.set(buffer_pos, val);
                if val > 0 {
                    self.num_writes.set(buffer_pos, self.num_writes.get(buffer_pos) + 1);
                }
            }
        }
    }

    fn clear(&self) {
        unsafe {
            for position in self.buffer_position as usize..(self.buffer_position + BPM_CURRENT as f32 / BPM_MIN as f32) as usize {
                let buffer_pos = position % self.size;
                self.num_writes.set(buffer_pos, 1);
                self.buffer.set(buffer_pos, 0);
            }
        }
    }

    pub fn get(&self) -> (u32, u32) {
        let mut mean = (0, 0);
        unsafe {
            let n_values = (BPM_CURRENT as f32 / BPM_MIN as f32) as usize;
            for position in self.buffer_position as usize..self.buffer_position as usize + n_values {
                let buffer_pos = position % self.size;
                mean.0 += self.buffer.get(buffer_pos) as usize;
                mean.1 += self.num_writes.get(buffer_pos) as usize;
            }
            return ((mean.0 / n_values) as u32, (mean.1 / n_values) as u32);
        }
    }

    pub fn set_state(&mut self, new_state: RecorderState) {
        self.state = new_state;
    }
}
