#![allow(dead_code)]

use crate::consts::*;

pub struct Util {}

#[derive(Copy, Clone)]
pub struct Matrix3x3 {
    data: [[f32; 3]; 3],
}

static mut RNG: i32 = 69254302;

impl Util {
    //pub const fn gen_sin() -> [f32;128] {
    //    let mut r = [0f32;128];
    //    for i in 0..128 {
    //        r[i] = math::sinf((i as f32 * TAU)/128.0);
    //    }
    //    r
    //}

    pub fn sin(v: f32) -> f32 {
        SIN_CACHE[(128.0 * ((v % TAU) / TAU)) as usize]
    }

    pub fn sin3(v: f32) -> f32 {
        SIN3_CACHE[(128.0 * ((v % TAU) / TAU)) as usize]
    }
    /*
    pub fn saw(v: f32) -> f32 {
        ((v as f32 * 2.0) / PI) % 2.0 - 1.0
    }

    pub fn triangle(v: f32) -> f32 {
        let r = ((v) / PI) % 2.0;
        if r > 1.0 { 2.0 * (2.0 - r) - 1.0 } else { 2.0*r - 1.0 }

    }

    pub fn square(v: f32) -> f32 {
        if ((v as f32) / PI) % 2.0 < 1.0 {
            -1.0
        } else {
            1.0
        }
    }
    */

    pub fn noise(_v: f32) -> f32 {
        Util::rand()
    }

    pub fn peak(v: f32) -> f32 {
        if ((v as f32 * 2.0) / PI) % 2.0 < 0.3 {
            1.0
        } else {
            0.0
        }
    }

    pub fn zero(_v: f32) -> f32 {
        0.0
    }

    pub fn rand() -> f32 {
        // https://stackoverflow.com/a/23875298/1761622
        unsafe {
            //RNG = RNG ^ (RNG >> 13);
            //RNG = RNG ^ (RNG >> 18);
            //RNG = RNG & 0x7FFFFFFF;
            //return (RNG as f32) / (0x7FFFFFFF as f32);

            RNG = ((RNG * 1103515245) + 12345) & 0x7fffffff;
            return (RNG % 0xFFFFFF) as f32 / (0xFFFFFF as f32);
        }
    }

    pub fn min(a: i32, b: i32) -> i32 {
        if a < b {
            a
        } else {
            b
        }
    }

    pub fn min_u32(a: u32, b: u32) -> u32 {
        if a < b {
            a
        } else {
            b
        }
    }

    pub fn max(a: i32, b: i32) -> i32 {
        if a > b {
            a
        } else {
            b
        }
    }

    pub fn max_u32(a: u32, b: u32) -> u32 {
        if a > b {
            a
        } else {
            b
        }
    }

    pub fn fmax(a: f32, b: f32) -> f32 {
        if a > b {
            a
        } else {
            b
        }
    }

    pub fn in_rect(x: u16, y: u16, w: u16, h: u16, point: (f32, f32)) -> bool {
        if point.0 < f32::from(x) {
            return false;
        }
        if f32::from(x + w) < point.0 {
            return false;
        }
        if point.1 < f32::from(y) {
            return false;
        }
        if (f32::from(y + h)) < point.1 {
            return false;
        }
        true
    }

    pub fn in_rectf(x: f32, y: f32, w: f32, h: f32, point: (f32, f32)) -> bool {
        if point.0 < x {
            return false;
        }
        if x + w < point.0 {
            return false;
        }
        if point.1 < y {
            return false;
        }
        if y + h < point.1 {
            return false;
        }
        true
    }

    pub fn dist8(mut x: i32, mut y: i32, inc: i32) -> f32 {
        x = 7 - x;
        y = 7 - y;
        if x < 0 {
            x = -x - 1;
        }
        if y < 0 {
            y = -y - 1;
        }
        x = (x - inc + 262144) % 8;
        y = (y - inc + 262144) % 8;
        return DIST_CACHE[(y * 8 + x) as usize];
    }
}

impl Matrix3x3 {
    pub fn identity() -> Self {
        Self {
            data: [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]],
        }
    }

    pub fn empty() -> Self {
        Self { data: [[0.0; 3]; 3] }
    }

    pub fn new(matrix: [[f32; 3]; 3]) -> Self {
        Self { data: matrix }
    }

    pub fn get(&self, x: usize, y: usize) -> f32 {
        self.data[y][x]
    }

    pub fn mult(&self, vec: (f32, f32)) -> (f32, f32) {
        (
            self.data[0][0] * vec.0 + self.data[0][1] * vec.1 + self.data[0][2] * 1.0,
            self.data[1][0] * vec.0 + self.data[1][1] * vec.1 + self.data[1][2] * 1.0,
        )
    }

    pub fn matrix_mult(self, matrix: [[f32; 3]; 3]) -> Self {
        Matrix3x3::new([
            [
                self.data[0][0] * matrix[0][0] + self.data[0][1] * matrix[1][0] + self.data[0][2] * matrix[2][0],
                self.data[0][0] * matrix[0][1] + self.data[0][1] * matrix[1][1] + self.data[0][2] * matrix[2][1],
                self.data[0][0] * matrix[0][2] + self.data[0][1] * matrix[1][2] + self.data[0][2] * matrix[2][2],
            ],
            [
                self.data[1][0] * matrix[0][0] + self.data[1][1] * matrix[1][0] + self.data[1][2] * matrix[2][0],
                self.data[1][0] * matrix[0][1] + self.data[1][1] * matrix[1][1] + self.data[1][2] * matrix[2][1],
                self.data[1][0] * matrix[0][2] + self.data[1][1] * matrix[1][2] + self.data[1][2] * matrix[2][2],
            ],
            [
                self.data[2][0] * matrix[0][0] + self.data[2][1] * matrix[1][0] + self.data[2][2] * matrix[2][0],
                self.data[2][0] * matrix[0][1] + self.data[2][1] * matrix[1][1] + self.data[2][2] * matrix[2][1],
                self.data[2][0] * matrix[0][2] + self.data[2][1] * matrix[1][2] + self.data[2][2] * matrix[2][2],
            ],
        ])
    }

    pub fn invert(&self) -> Matrix3x3 {
        let mut aug = [[0f32; 6]; 3];
        for (i, item) in aug.iter_mut().enumerate() {
            item[..3].clone_from_slice(&self.data[i][..3]);
            // augment by identity matrix to right
            item[i + 3] = 1.0;
        }

        self.to_reduced_row_echelon_form(&mut aug);

        let mut inv = Matrix3x3::empty();
        // remove identity matrix to left
        for (i, item) in aug.iter().enumerate() {
            inv.data[i][0..(6 - 3)].clone_from_slice(&item[3..6])
        }
        inv
    }

    fn to_reduced_row_echelon_form(&self, this: &mut [[f32; 6]; 3]) {
        let mut lead = 0;
        for r in 0..3 {
            if 6 <= lead {
                return;
            }
            let mut i = r;

            while this[i][lead] == 0.0 {
                i += 1;
                if 3 == i {
                    i = r;
                    lead += 1;
                    if 6 == lead {
                        return;
                    }
                }
            }

            this.swap(i, r);

            if this[r][lead] != 0.0 {
                let div = this[r][lead];
                for j in 0..6 {
                    this[r][j] /= div;
                }
            }

            for k in 0..3 {
                if k != r {
                    let mult = this[k][lead];
                    for j in 0..6 {
                        this[k][j] -= this[r][j] * mult;
                    }
                }
            }

            lead += 1;
        }
    }
}
